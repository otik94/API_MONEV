<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_laporan_tahunan extends Model {
    protected $table = 'tbl_laporan_tahunan';
    
    protected $fillable = ['status'];

    public $timestamps = false;

    public static $rules = [
        'status' => 'required'
    ];

}
