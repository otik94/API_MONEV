<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Access_pemohon extends Model {
    protected $table = 'access_pemohon';

    protected $fillable = [
      'id_perusahaan',
      'username',
      'password',
      'email'

  ];

    public $timestamps = false;

    public static $rules = [
        'id_perusahaan' => 'required',
        'username' => 'required',
        'password' => 'required',
        'email' => 'required',

    ];

}
