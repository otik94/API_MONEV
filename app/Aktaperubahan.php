<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Aktaperubahan extends Model {
    protected $table = 'aktaperubahan';
    protected $primaryKey = 'IDPERUSAHAANFINAL';
    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'NOPERUBAHAN',
      'TANGGALPERUBAHAN',
      'NAMAINSTANSI',
      'DOMISILIINSTANSI',
      'tgl_perpanjangan',
      'tgl_perubahan'

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NOPERUBAHAN' => 'required',
    //     'TANGGALPERUBAHAN' => 'required',
    //     'NAMAINSTANSI' => 'required',
    //     'DOMISILIINSTANSI' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
