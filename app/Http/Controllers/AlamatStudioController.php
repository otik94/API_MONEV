<?php namespace App\Http\Controllers;

use App\alamatstudio;
use Illuminate\Http\Request;

class AlamatStudioController extends Controller
{


    public function updateAlamatStudio(Request $request,$IDPERUSAHAANFINAL){
        $AlamatStudio         = AlamatStudio::find($IDPERUSAHAANFINAL);
        $AlamatStudio->PROPSTUDIO  = $request->input('PROPSTUDIO');
        $AlamatStudio->ALAMATSTUDIO = $request->input('ALAMATSTUDIO');
        $AlamatStudio->KABSTUDIO   = $request->input('KABSTUDIO');
        $AlamatStudio->KECSTUDIO = $request->input('KECSTUDIO');
        $AlamatStudio->KELSTUDIO   = $request->input('KELSTUDIO');
        $AlamatStudio->KODEPOS = $request->input('KODEPOS');
        $AlamatStudio->NOTELP   = $request->input('NOTELP');
        $AlamatStudio->save();

        // return response()->json($Contact);
        return response()->json([
            'message' => 'Successfull update Alamat Studio'
        ]);
    }

}
