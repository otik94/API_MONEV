<?php namespace App\Http\Controllers;

use App\tbl_laporan_tahunan;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index()
    {
        $status = tbl_laporan_tahunan::all();
        return response()->json($status);
    }

    // public function read(Request $request, $id)
    // {
    //   $status = tbl_laporan_tahunan::where('id',$id)->first();
    //   if($status !== null){
    //     $res['success'] = true;
    //     $res['result'] = $status;
    //
    //     return response($res);
    //   }else {
    //     $res['success'] = false;
    //     $res['result'] = 'status not found!';
    //
    //     return response($res);
    //   }
    // }
    public function getStatus(Request $request, $id)
    {
      // $status = tbl_laporan_tahunan::where('id',$id)->where('status','==','1')->get();
      // $status = tbl_laporan_tahunan::where('status','=','1',$id)->first();
      // dd($status);
      // if($status !=null){
      // $status = tbl_laporan_tahunan::where('id',$id)->where('status','=','1')->first();
      if($status = tbl_laporan_tahunan::where('id',$id)->where('status','=','1')->first()){
        $res['success'] = true;
        $res['result'] = 'lolos';

        return response($res);
      }else if($status = tbl_laporan_tahunan::where('id',$id)->where('status','=','2')->first()){
        $res['success'] = true;
        $res['result'] = 'tidak lolos';

        return response($res);
      }else if($status = tbl_laporan_tahunan::where('id',$id)->where('status','=','0')->first()){
        $res['success'] = true;
        $res['result'] = 'belum ada keputusan';

        return response($res);
      }
      else {
        $res['success'] = false;
        $res['result'] = 'data tidak ada!';

        return response($res);
      }
    }

 //    public function read($id){
 //   $articles = tbl_laporan_tahunan::where('id','=', $id)->where('status','=','2')->get();
 //
 //   if($articles){
 //     return response()->json(['data' => $articles], 200);
 //   }
 //
 //   return response()->json(['data' => 'Articles in this category not found'], 404);
 // }



    // public function index(Request $request)
    // {
    //   $status = new tbl_laporan_tahunan;
    //
    //   $res['success'] = true;
    //   $res['result'] = $status->all();
    //
    //   return response($res);
    // }

    // public function store(Request $request)
    // {
    //     Category::create($request->all());
    //     return response()->json([
    //        'message' => 'Successfull create new category'
    //     ]);
    // }

    // public function show($id)
    // {
    //     $category = Category::find($id);
    //     return response()->json($category);
    // }

    // public function update(Request $request, $id)
    // {
    //     $category = Category::find($id);
    //     $category->update($request->all());
    //
    //     return response()->json([
    //         'message' => 'Successfull update category'
    //     ]);
    // }

    // public function delete($id)
    // {
    //     Category::destroy($id);
    //
    //     return response()->json([
    //         'message' => 'Successfull delete category'
    //     ]);
    // }

}
