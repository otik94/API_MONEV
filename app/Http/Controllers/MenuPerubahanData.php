<?php namespace App\Http\Controllers;
use App\DB;
use App\tbl_laporan_tahunan;
use App\ContactPerson;
use App\AlamatStudio;
use App\Alamatkantor;
use App\Alamatpemancar;
use App\Aktapendirian;
use App\Aktaperubahan;
use App\AspekModallpb;
use App\AspekPermodalan;
use App\AspekPermodalanKom;
use App\AspekModalKomSumber;
use App\AlamatStasiunPengendali;
use App\DpPemberitaan;
use App\DpSiaran;
use App\DpTataUsaha;
use App\DpTeknikStudio;
use App\DpTeknikTransmisi;
use App\DpTotalPengurus;
use App\KetuaDpk;
use App\AnggotaDpk;
use App\KetuaDPengawas;
use App\AnggotaDPengawas;
use App\AnggotaDPengawas2;
use App\KetuaPendiri;
use App\Pendiri;
use App\Direktur;
use App\Direktur_2;
use App\KomisarisUtama;
use App\Komisaris;
use App\Komisaris2;
use App\PjbIdKeu;
use App\PjbIdPemberitaan;
use App\PjbIdSiaran;
use App\PjbIdTeknik;
use App\PjbIdUsaha;
use App\pendirian_lpplokal;
use App\peng_aktapendirian;
use App\peng_aktaperubahan;
use App\SuratKetDomisili;
use App\SistemModulasi;
use App\StasiunDistribusi;
use App\StasiunPemancar;
use App\SpekSatelit;
use App\SatelitPenyiaran;
use App\SpekPerlRadio;
use App\Transponder;
use App\PeralatanPemancar;
use App\Feeder;
use App\FormatSiaran;
use App\SurveyJk;
// use App\SurveyUsia;
use App\SurveyPekerjaan;
use App\SurveyPendidikan;
use App\surveyekonomi;
use App\SumberMateri;
use App\WaktusiaranLpb;
use App\PersentaseMataSiaran;
use App\PersentaseSiaran;
use App\PersentaseSiaranMusik;
use App\PersentaseSiaranLpb;
use App\MateriSiaran;
use App\WaktuSiaran;
use App\KomposisiPemegangSaham;
use App\Penyelenggara;
use App\tbl_ipp_prinsip;
use App\tbl_ipp_tetap;
use App\SumberMateriSiaranLpb;
use App\IklanKomersial;
use App\TotalKanalUmum;
use App\TotalKanalBerita;
use App\TotalKanalMusik;
use App\TotalKanalOlahraga;
use App\TotalKanalPendidikan;
use App\TotalKanalDakwah;
use App\TotalKanalFilm;
use App\TotalKanalLainnya;
use App\PemusatanKepemilikanSilang;
use App\PemegangSaham;
use Illuminate\Http\Request;

class MenuPerubahanData extends Controller
{
  public function index()
  {
    $status = tbl_laporan_tahunan::all();
    return response()->json($status);
  }

public function update_tbl_ipp_tetap(Request $request,$IDPERUSAHAANFINAL){
  $tbl_ipp_tetap    = tbl_ipp_tetap::find($IDPERUSAHAANFINAL);
  $tbl_ipp_tetap->id_perusahaan  = $request->input('id_perusahaan_ipp_tetap');
  $tbl_ipp_tetap->no_ipp_tetap = $request->input('no_ipp_tetap');
  $tbl_ipp_tetap->tgl_ipp_tetap = $request->input('tgl_ipp_tetap');
  $tbl_ipp_tetap->tgl_penetapan = $request->input('tgl_ipp_penetapan');
  $tbl_ipp_tetap->tgl_perpanjangan = $request->input('tgl_perpanjangan_ipp_tetap');
  $tbl_ipp_tetap->save();
  return response()->json([
    'message' => 'Successfull update data'
  ]);
}

  public function updateContactPerson(Request $request,$IDPERUSAHAANFINAL){
    $Contact         = ContactPerson::find($IDPERUSAHAANFINAL);
    $Contact->NAMA  = $request->input('NAMA_CP');
    $Contact->NOTELP = $request->input('NOTELP_CP');
    $Contact->NOFAX   = $request->input('NOFAX_CP');
    $Contact->NOHP = $request->input('NOHP_CP');
    $Contact->EMAIL   = $request->input('EMAIL_CP');
    // $Contact->update($request->all());
    $Contact->save();

            $AlamatStudio         = AlamatStudio::find($IDPERUSAHAANFINAL);
            $AlamatStudio->PROPSTUDIO  = $request->input('PROPSTUDIO');
            $AlamatStudio->ALAMATSTUDIO = $request->input('ALAMATSTUDIO');
            $AlamatStudio->KABSTUDIO   = $request->input('KABSTUDIO');
            $AlamatStudio->KECSTUDIO = $request->input('KECSTUDIO');
            $AlamatStudio->KELSTUDIO   = $request->input('KELSTUDIO');
            $AlamatStudio->KODEPOS = $request->input('KODEPOSSTUDIO');
            $AlamatStudio->NOTELP   = $request->input('NOTELPSTUDIO');
            $AlamatStudio->save();

            $AlamatKantor         = Alamatkantor::find($IDPERUSAHAANFINAL);
            $AlamatKantor->NMPERUSAHAAN  = $request->input('NMPERUSAHAAN');
            $AlamatKantor->ALAMATKANTOR = $request->input('ALAMATKANTOR');
            $AlamatKantor->PROPKANTOR   = $request->input('PROPKANTOR');
            $AlamatKantor->KABKANTOR = $request->input('KABKANTOR');
            $AlamatKantor->KECKANTOR   = $request->input('KECKANTOR');
            $AlamatKantor->KELKANTOR = $request->input('KELKANTOR');
            $AlamatKantor->KODEPOS   = $request->input('KODEPOSKANTOR');
            $AlamatKantor->NOTELPKANTOR   = $request->input('NOTELPKANTOR');
            $AlamatKantor->NOHPKANTOR = $request->input('NOHPKANTOR');
            $AlamatKantor->NPWP   = $request->input('NPWP');
            $AlamatKantor->WEBSITE = $request->input('WEBSITE');
            $AlamatKantor->ZONA   = $request->input('ZONA');
            $AlamatKantor->save();
            //
            $AlamatPemancar         = Alamatpemancar::find($IDPERUSAHAANFINAL);
            $AlamatPemancar->ALAMATPEMANCAR  = $request->input('ALAMATPEMANCAR');
            $AlamatPemancar->PROPPEMANCAR = $request->input('PROPPEMANCAR');
            $AlamatPemancar->KABPEMANCAR   = $request->input('KABPEMANCAR');
            $AlamatPemancar->KECPEMANCAR = $request->input('KECPEMANCAR');
            $AlamatPemancar->KELPEMANCAR   = $request->input('KELPEMANCAR');
            $AlamatPemancar->KODEPOS = $request->input('KODEPOSPEMANCAR');
            $AlamatPemancar->FAX   = $request->input('FAX_AP');
            $AlamatPemancar->TINGGILOKASI   = $request->input('TINGGILOKASI_AP');
            $AlamatPemancar->KOORDINAT = $request->input('KOORDINAT_AP');
            $AlamatPemancar->longitude   = $request->input('longitude');
            $AlamatPemancar->latitude = $request->input('latitude');
            $AlamatPemancar->NOTELP   = $request->input('NOTELP_AP');
            $AlamatPemancar->long_deg   = $request->input('long_deg');
            $AlamatPemancar->lat_deg = $request->input('lat_deg');
            $AlamatPemancar->long_minutes   = $request->input('long_minutes');
            $AlamatPemancar->lat_minutes   = $request->input('lat_minutes');
            $AlamatPemancar->long_seconds = $request->input('long_seconds');
            $AlamatPemancar->lat_seconds   = $request->input('lat_seconds');
            $AlamatPemancar->long_direction = $request->input('long_direction');
            $AlamatPemancar->lat_direction   = $request->input('lat_direction');
            $AlamatPemancar->save();

            $AktaPendirian         = Aktapendirian::find($IDPERUSAHAANFINAL);
            $AktaPendirian->NOAKTA  = $request->input('NOAKTA_AP');
            $AktaPendirian->TANGGALPENDIRIAN = $request->input('TANGGALPENDIRIAN_AP');
            $AktaPendirian->NAMANOTARIS   = $request->input('NAMANOTARIS_AP');
            $AktaPendirian->DOMISILINOTARIS = $request->input('DOMISILINOTARIS_AP');
            $AktaPendirian->save();

            $AktaPerubahan         = Aktaperubahan::find($IDPERUSAHAANFINAL);
            $AktaPerubahan->NOPERUBAHAN  = $request->input('NOPERUBAHAN_APT');
            $AktaPerubahan->TANGGALPERUBAHAN = $request->input('TANGGALPERUBAHAN_APT');
            $AktaPerubahan->NAMAINSTANSI   = $request->input('NAMAINSTANSI_APT');
            $AktaPerubahan->DOMISILIINSTANSI = $request->input('DOMISILIINSTANSI_APT');
            $AktaPerubahan->save();

            $AspekModallpb        = AspekModallpb::find($IDPERUSAHAANFINAL);
            $AspekModallpb->MODALDASAR  = $request->input('MODALDASARLLPB');
            $AspekModallpb->BANYAKSAHAM = $request->input('BANYAKSAHAMLLPB');
            $AspekModallpb->NOMINALSAHAM = $request->input('NOMINALSAHAMLLPB');
            $AspekModallpb->KOMPOSISI = $request->input('KOMPOSISILLPB');
            $AspekModallpb->MODALSETOR = $request->input('MODALSETORLLPB');
            $AspekModallpb->save();

            $AspekPermodalan        = AspekPermodalan::find($IDPERUSAHAANFINAL);
            $AspekPermodalan->MODALDASAR  = $request->input('MODALDASARPERMODALAN');
            $AspekPermodalan->JMLSAHAM = $request->input('JMLSAHAMPERMODALAN');
            $AspekPermodalan->NOMINALSAHAM = $request->input('NOMINALSAHAMPERMODALAN');
            $AspekPermodalan->KOMPOSISI = $request->input('KOMPOSISIPERMODALAN');
            $AspekPermodalan->MODALSETOR = $request->input('MODALSETORPERMODALAN');
            $AspekPermodalan->MODALDITEMPATKAN = $request->input('MODALDITEMPATKANPERMODALAN');
            $AspekPermodalan->save();

            $AspekPermodalanKom        = AspekPermodalanKom::find($IDPERUSAHAANFINAL);
            $AspekPermodalanKom->SIMPANANPOKOK  = $request->input('SIMPANANPOKOK');
            $AspekPermodalanKom->SIMPANANSUKARELA = $request->input('SIMPANANSUKARELA');
            $AspekPermodalanKom->JMLANGGOTA = $request->input('JMLANGGOTA');
            $AspekPermodalanKom->JMLMODALAWAL = $request->input('JMLMODALAWAL');
            $AspekPermodalanKom->save();

            $AspekModalKomSumber       = AspekModalKomSumber::find($IDPERUSAHAANFINAL);
            $AspekModalKomSumber->SUMBANGAN  = $request->input('SUMBANGAN');
            $AspekModalKomSumber->HIBAH = $request->input('HIBAH');
            $AspekModalKomSumber->SPONSOR = $request->input('SPONSOR');
            $AspekModalKomSumber->LAINNYA = $request->input('LAINNYA_AP');
            $AspekModalKomSumber->SEBUTKAN = $request->input('SEBUTKAN_AP');
            $AspekModalKomSumber->save();
    //
            $AlamatStasiunPengendali       = AlamatStasiunPengendali::find($IDPERUSAHAANFINAL);
            $AlamatStasiunPengendali->PROPSTASIUN  = $request->input('PROPSTASIUN');
            $AlamatStasiunPengendali->ALAMATSTASIUN = $request->input('ALAMATSTASIUN');
            $AlamatStasiunPengendali->KABSTASIUN = $request->input('KABSTASIUN');
            $AlamatStasiunPengendali->KECSTASIUN = $request->input('KECSTASIUN');
            $AlamatStasiunPengendali->KELSTASIUN = $request->input('KELSTASIUN');
            $AlamatStasiunPengendali->KODEPOS  = $request->input('KODEPOSSTASIUN');
            $AlamatStasiunPengendali->NOTELP = $request->input('NOTELPSTASIUN');
            $AlamatStasiunPengendali->FAX = $request->input('FAXSTASIUN');
            $AlamatStasiunPengendali->TINGGILOKASI = $request->input('TINGGILOKASI_AS');
            $AlamatStasiunPengendali->KOORDINAT = $request->input('KOORDINAT_AS');
            $AlamatStasiunPengendali->save();

            $DpPemberitaan       = DpPemberitaan::find($IDPERUSAHAANFINAL);
            $DpPemberitaan->PSTETAP  = $request->input('PSTETAPPEMBERITAAN');
            $DpPemberitaan->PSTIDAK = $request->input('PSTIDAKPEMBERITAAN');
            $DpPemberitaan->SARJANATETAP = $request->input('SARJANATETAPPEMBERITAAN');
            $DpPemberitaan->SARJANATIDAK = $request->input('SARJANATIDAKPEMBERITAAN');
            $DpPemberitaan->DIPLOMATETAP = $request->input('DIPLOMATETAPPEMBERITAAN');
            $DpPemberitaan->DIPLOMATIDAK  = $request->input('DIPLOMATIDAKPEMBERITAAN');
            $DpPemberitaan->SLTATETAP = $request->input('SLTATETAPPEMBERITAAN');
            $DpPemberitaan->SLTATIDAK = $request->input('SLTATIDAKPEMBERITAAN');
            $DpPemberitaan->SLTPTETAP = $request->input('SLTPTETAPPEMBERITAAN');
            $DpPemberitaan->SLTPTIDAK = $request->input('SLTPTIDAKPEMBERITAAN');
            $DpPemberitaan->SDTETAP = $request->input('SDTETAPPEMBERITAAN');
            $DpPemberitaan->SDTIDAK = $request->input('SDTIDAKPEMBERITAAN');
            $DpPemberitaan->TOTALTETAP = $request->input('TOTALTETAPPEMBERITAAN');
            $DpPemberitaan->TOTALTIDAK = $request->input('TOTALTIDAKPEMBERITAAN');
            $DpPemberitaan->save();
            //masih ada bug
            $DpSiaran       = DpSiaran::find($IDPERUSAHAANFINAL);
            $DpSiaran->PSTETAP  = $request->input('PSTETAPSIARAN');
            $DpSiaran->PSTIDAK = $request->input('PSTIDAKSIARAN');
            $DpSiaran->SARJANATETAP = $request->input('SARJANATETAPSIARAN');
            $DpSiaran->SARJANATIDAK = $request->input('SARJANATIDAKSIARAN');
            $DpSiaran->DIPLOMATETAP = $request->input('DIPLOMATETAPSIARAN');
            $DpSiaran->DIPLOMATIDAK  = $request->input('DIPLOMATIDAKSIARAN');
            $DpSiaran->SLTATETAP = $request->input('SLTATETAPSIARAN');
            $DpSiaran->SLTATIDAK = $request->input('SLTATIDAKSIARAN');
            $DpSiaran->SLTPTETAP = $request->input('SLTPTETAPSIARAN');
            $DpSiaran->SLTPTIDAK = $request->input('SLTPTIDAKSIARAN');
            $DpSiaran->SDTETAP = $request->input('SDTETAPSIARAN');
            $DpSiaran->SDTIDAK = $request->input('SDTIDAKSIARAN');
            $DpSiaran->TOTALTETAP = $request->input('TOTALTETAPSIARAN');
            $DpSiaran->TOTALTIDAK = $request->input('TOTALTIDAKSIARAN');
            $DpSiaran->save();
            //masih ada bug
            $DpTataUsaha       = DpTataUsaha::find($IDPERUSAHAANFINAL);
            $DpTataUsaha->PSTETAP  = $request->input('PSTETAPTATAUSAHA');
            $DpTataUsaha->PSTIDAK = $request->input('PSTIDAKTATAUSAHA');
            $DpTataUsaha->SARJANATETAP = $request->input('SARJANATETAPTATAUSAHA');
            $DpTataUsaha->SARJANATIDAK = $request->input('SARJANATIDAKTATAUSAHA');
            $DpTataUsaha->DIPLOMATETAP = $request->input('DIPLOMATETAPTATAUSAHA');
            $DpTataUsaha->DIPLOMATIDAK  = $request->input('DIPLOMATIDAKTATAUSAHA');
            $DpTataUsaha->SLTATETAP = $request->input('SLTATETAPTATAUSAHA');
            $DpTataUsaha->SLTATIDAK = $request->input('SLTATIDAKTATAUSAHA');
            $DpTataUsaha->SLTPTETAP = $request->input('SLTPTETAPTATAUSAHA');
            $DpTataUsaha->SLTPTIDAK = $request->input('SLTPTIDAKTATAUSAHA');
            $DpTataUsaha->SDTETAP = $request->input('SDTETAPTATAUSAHA');
            $DpTataUsaha->SDTIDAK = $request->input('SDTIDAKTATAUSAHA');
            $DpTataUsaha->TOTALTETAP = $request->input('TOTALTETAPTATAUSAHA');
            $DpTataUsaha->TOTALTIDAK = $request->input('TOTALTIDAKTATAUSAHA');
            $DpTataUsaha->save();

            $DpTeknikStudio       = DpTeknikStudio::find($IDPERUSAHAANFINAL);
            $DpTeknikStudio->PSTETAP  = $request->input('PSTETAPTEKNIKSTUDIO');
            $DpTeknikStudio->PSTIDAK = $request->input('PSTIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->SARJANATETAP = $request->input('SARJANATETAPTEKNIKSTUDIO');
            $DpTeknikStudio->SARJANATIDAK = $request->input('SARJANATIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->DIPLOMATETAP = $request->input('DIPLOMATETAPTEKNIKSTUDIO');
            $DpTeknikStudio->DIPLOMATIDAK  = $request->input('DIPLOMATIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->SLTATETAP = $request->input('SLTATETAPTEKNIKSTUDIO');
            $DpTeknikStudio->SLTATIDAK = $request->input('SLTATIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->SLTPTETAP = $request->input('SLTPTETAPTEKNIKSTUDIO');
            $DpTeknikStudio->SLTPTIDAK = $request->input('SLTPTIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->SDTETAP = $request->input('SDTETAPTEKNIKSTUDIO');
            $DpTeknikStudio->SDTIDAK = $request->input('SDTIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->TOTALTETAP = $request->input('TOTALTETAPTEKNIKSTUDIO');
            $DpTeknikStudio->TOTALTIDAK = $request->input('TOTALTIDAKTEKNIKSTUDIO');
            $DpTeknikStudio->save();
            //
            $DpTeknikTransmisi       = DpTeknikTransmisi::find($IDPERUSAHAANFINAL);
            $DpTeknikTransmisi->PSTETAP  = $request->input('PSTETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->PSTIDAK = $request->input('PSTIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SARJANATETAP = $request->input('SARJANATETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SARJANATIDAK = $request->input('SARJANATIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->DIPLOMATETAP = $request->input('DIPLOMATETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->DIPLOMATIDAK  = $request->input('DIPLOMATIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SLTATETAP = $request->input('SLTATETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SLTATIDAK = $request->input('SLTATIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SLTPTETAP = $request->input('SLTPTETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SLTPTIDAK = $request->input('SLTPTIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SDTETAP = $request->input('SDTETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->SDTIDAK = $request->input('SDTIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->TOTALTETAP = $request->input('TOTALTETAPTEKNIKTRANSMISI');
            $DpTeknikTransmisi->TOTALTIDAK = $request->input('TOTALTIDAKTEKNIKTRANSMISI');
            $DpTeknikTransmisi->save();

            $DpTotalPengurus       = DpTotalPengurus::find($IDPERUSAHAANFINAL);
            $DpTotalPengurus->PSTETAP  = $request->input('PSTETAPTOTALPENGURUS');
            $DpTotalPengurus->PSTIDAK = $request->input('PSTIDAKTOTALPENGURUS');
            $DpTotalPengurus->SARJANATETAP = $request->input('SARJANATETAPTOTALPENGURUS');
            $DpTotalPengurus->SARJANATIDAK = $request->input('SARJANATIDAKTOTALPENGURUS');
            $DpTotalPengurus->DIPLOMATETAP = $request->input('DIPLOMATETAPTOTALPENGURUS');
            $DpTotalPengurus->DIPLOMATIDAK  = $request->input('DIPLOMATIDAKTOTALPENGURUS');
            $DpTotalPengurus->SLTATETAP = $request->input('SLTATETAPTOTALPENGURUS');
            $DpTotalPengurus->SLTATIDAK = $request->input('SLTATIDAKTOTALPENGURUS');
            $DpTotalPengurus->SLTPTETAP = $request->input('SLTPTETAPTOTALPENGURUS');
            $DpTotalPengurus->SLTPTIDAK = $request->input('SLTPTIDAKTOTALPENGURUS');
            $DpTotalPengurus->SDTETAP = $request->input('SDTETAPTOTALPENGURUS');
            $DpTotalPengurus->SDTIDAK = $request->input('SDTIDAKTOTALPENGURUS');
            $DpTotalPengurus->TOTALTETAP = $request->input('TOTALTETAPTOTALPENGURUS');
            $DpTotalPengurus->TOTALTIDAK = $request->input('TOTALTIDAKTOTALPENGURUS');
            $DpTotalPengurus->save();

            $KetuaDpk       = KetuaDpk::find($IDPERUSAHAANFINAL);
            $KetuaDpk->NAMAKETUA  = $request->input('DPK_NAMA');
            $KetuaDpk->TTLKETUA = $request->input('DPK_TTL');
            $KetuaDpk->TMP_LAHIR = $request->input('DPK_TMP_LAHIR');
            $KetuaDpk->KEWARGANEGARAAN = $request->input('DPK_KEWARGANEGARAAN');
            $KetuaDpk->PENDIDIKAN = $request->input('DPK_PENDIDIKAN');
            $KetuaDpk->NOTELP  = $request->input('DPK_TELP_KANTOR');
            $KetuaDpk->NOHP = $request->input('DPK_NOHP');
            $KetuaDpk->FAX = $request->input('DPK_FAX');
            $KetuaDpk->EMAIL = $request->input('DPK_EMAIL');
            $KetuaDpk->save();

            $AnggotaDpk       = AnggotaDpk::find($IDPERUSAHAANFINAL);
            $AnggotaDpk->NAMAKETUA  = $request->input('ADPK_NAMA');
            $AnggotaDpk->TTLKETUA = $request->input('ADPK_TTL');
            $AnggotaDpk->TMP_LAHIR = $request->input('ADPK_TMP_LAHIR');
            $AnggotaDpk->KEWARGANEGARAAN = $request->input('ADPK_KEWARGANEGARAAN');
            $AnggotaDpk->PENDIDIKAN = $request->input('ADPK_PENDIDIKAN');
            $AnggotaDpk->NOTELP  = $request->input('ADPK_TELP_KANTOR');
            $AnggotaDpk->NOHP = $request->input('ADPK_NOHP');
            $AnggotaDpk->FAX = $request->input('ADPK_FAX');
            $AnggotaDpk->EMAIL = $request->input('ADPK_EMAIL');
            $AnggotaDpk->save();

            $KetuaDPengawas       = KetuaDPengawas::find($IDPERUSAHAANFINAL);
            $KetuaDPengawas->NAMAKETUA  = $request->input('KDP_NAMA');
            $KetuaDPengawas->TTLKETUA = $request->input('KDP_TTL');
            $KetuaDPengawas->TMP_LAHIR = $request->input('KDP_TMP_LAHIR');
            $KetuaDPengawas->KEWARGANEGARAAN = $request->input('KDP_KEWARGANEGARAAN');
            $KetuaDPengawas->PENDIDIKAN = $request->input('KDP_PENDIDIKAN');
            $KetuaDPengawas->NOTELP  = $request->input('KDP_TELP_KANTOR');
            $KetuaDPengawas->NOHP = $request->input('KDP_NOHP');
            $KetuaDPengawas->FAX = $request->input('KDP_FAX');
            $KetuaDPengawas->EMAIL = $request->input('KDP_EMAIL');
            $KetuaDPengawas->save();

            $AnggotaDPengawas      = AnggotaDPengawas::find($IDPERUSAHAANFINAL);
            $AnggotaDPengawas->NAMAKETUA  = $request->input('ADP1_NAMA');
            $AnggotaDPengawas->TTLKETUA = $request->input('ADP1_TTL');
            $AnggotaDPengawas->TMP_LAHIR = $request->input('ADP1_TMP_LAHIR');
            $AnggotaDPengawas->KEWARGANEGARAAN = $request->input('ADP1_KEWARGANEGARAAN');
            $AnggotaDPengawas->PENDIDIKAN = $request->input('ADP1_PENDIDIKAN');
            $AnggotaDPengawas->NOTELP  = $request->input('ADP1_TELP_KANTOR');
            $AnggotaDPengawas->NOHP = $request->input('ADP1_NOHP');
            $AnggotaDPengawas->FAX = $request->input('ADP1_FAX');
            $AnggotaDPengawas->EMAIL = $request->input('ADP1_EMAIL');
            $AnggotaDPengawas->save();

            $AnggotaDPengawas2      = AnggotaDPengawas2::find($IDPERUSAHAANFINAL);
            $AnggotaDPengawas2->NAMAKETUA  = $request->input('ADP2_NAMA');
            $AnggotaDPengawas2->TTLKETUA = $request->input('ADP2_TTL');
            $AnggotaDPengawas2->TMP_LAHIR = $request->input('ADP2_TMP_LAHIR');
            $AnggotaDPengawas2->KEWARGANEGARAAN = $request->input('ADP2_KEWARGANEGARAAN');
            $AnggotaDPengawas2->PENDIDIKAN = $request->input('ADP2_PENDIDIKAN');
            $AnggotaDPengawas2->NOTELP  = $request->input('ADP2_TELP_KANTOR');
            $AnggotaDPengawas2->NOHP = $request->input('ADP2_NOHP');
            $AnggotaDPengawas2->FAX = $request->input('ADP2_FAX');
            $AnggotaDPengawas2->EMAIL = $request->input('ADP2_EMAIL');
            $AnggotaDPengawas2->save();

            $KetuaPendiri    = KetuaPendiri::find($IDPERUSAHAANFINAL);
            $KetuaPendiri->NAMAKETUA  = $request->input('KP_NAMA');
            $KetuaPendiri->TTLKETUA = $request->input('KP_TTL');
            $KetuaPendiri->TMP_LAHIR = $request->input('KP_TMP_LAHIR');
            $KetuaPendiri->KEWARGANEGARAAN = $request->input('KP_KEWARGANEGARAAN');
            $KetuaPendiri->PENDIDIKAN = $request->input('KP_PENDIDIKAN');
            $KetuaPendiri->NOTELP  = $request->input('KP_TELP_KANTOR');
            $KetuaPendiri->NOHP = $request->input('KP_NOHP');
            $KetuaPendiri->FAX = $request->input('KP_FAX');
            $KetuaPendiri->EMAIL = $request->input('KP_EMAIL');
            $KetuaPendiri->save();

            $Pendiri    = Pendiri::find($IDPERUSAHAANFINAL);
            $Pendiri->NAMAKETUA  = $request->input('P_NAMA');
            $Pendiri->TTLKETUA = $request->input('P_TTL');
            $Pendiri->TMP_LAHIR = $request->input('P_TMP_LAHIR');
            $Pendiri->KEWARGANEGARAAN = $request->input('P_KEWARGANEGARAAN');
            $Pendiri->PENDIDIKAN = $request->input('P_PENDIDIKAN');
            $Pendiri->NOTELP  = $request->input('P_TELP_KANTOR');
            $Pendiri->NOHP = $request->input('P_NOHP');
            $Pendiri->FAX = $request->input('P_FAX');
            $Pendiri->EMAIL = $request->input('P_EMAIL');
            $Pendiri->save();

            $Direktur    = Direktur::find($IDPERUSAHAANFINAL);
            $Direktur->NAMAKETUA  = $request->input('DU_NAMA');
            $Direktur->TTLKETUA = $request->input('DU_TTL');
            $Direktur->TMP_LAHIR = $request->input('DU_TMP_LAHIR');
            $Direktur->KEWARGANEGARAAN = $request->input('DU_KEWARGANEGARAAN');
            $Direktur->PENDIDIKAN = $request->input('DU_PENDIDIKAN');
            $Direktur->NOTELP  = $request->input('DU_TELP_KANTOR');
            $Direktur->NOHP = $request->input('DU_NOHP');
            $Direktur->FAX = $request->input('DU_FAX');
            $Direktur->EMAIL = $request->input('DU_EMAIL');
            $Direktur->save();

            $Direktur_2    = Direktur_2::find($IDPERUSAHAANFINAL);
            $Direktur_2->NAMAKETUA  = $request->input('D_NAMA');
            $Direktur_2->TTLKETUA = $request->input('D_TTL');
            $Direktur_2->TMP_LAHIR = $request->input('D_TMP_LAHIR');
            $Direktur_2->KEWARGANEGARAAN = $request->input('D_KEWARGANEGARAAN');
            $Direktur_2->PENDIDIKAN = $request->input('D_PENDIDIKAN');
            $Direktur_2->NOTELP  = $request->input('D_TELP_KANTOR');
            $Direktur_2->NOHP = $request->input('D_NOHP');
            $Direktur_2->FAX = $request->input('D_FAX');
            $Direktur_2->EMAIL = $request->input('D_EMAIL');
            $Direktur_2->save();

            $KomisarisUtama    = KomisarisUtama::find($IDPERUSAHAANFINAL);
            $KomisarisUtama->NAMAKOMISARIS  = $request->input('KU_NAMA');
            $KomisarisUtama->TTLKOMISARIS = $request->input('KU_TTL');
            $KomisarisUtama->TMP_LAHIR = $request->input('KU_TMP_LAHIR');
            $KomisarisUtama->KEWARGANEGARAAN = $request->input('KU_KEWARGANEGARAAN');
            $KomisarisUtama->PENDIDIKAN = $request->input('KU_PENDIDIKAN');
            $KomisarisUtama->NOTELP  = $request->input('KU_TELP_KANTOR');
            $KomisarisUtama->NOHP = $request->input('KU_NOHP');
            $KomisarisUtama->FAX = $request->input('KU_FAX');
            $KomisarisUtama->EMAIL = $request->input('KU_EMAIL');
            $KomisarisUtama->save();

            $Komisaris    = Komisaris::find($IDPERUSAHAANFINAL);
            $Komisaris->NAMAKOMISARIS  = $request->input('KOMISARIS_NAMA');
            $Komisaris->TTLKOMISARIS = $request->input('KOMISARIS_TTL');
            $Komisaris->TMP_LAHIR = $request->input('KOMISARIS_TMP_LAHIR');
            $Komisaris->KEWARGANEGARAAN = $request->input('KOMISARIS_KEWARGANEGARAAN');
            $Komisaris->PENDIDIKAN = $request->input('KOMISARIS_PENDIDIKAN');
            $Komisaris->NOTELP  = $request->input('KOMISARIS_TELP_KANTOR');
            $Komisaris->NOHP = $request->input('KOMISARIS_NOHP');
            $Komisaris->FAX = $request->input('KOMISARIS_FAX');
            $Komisaris->EMAIL = $request->input('KOMISARIS_EMAIL');
            $Komisaris->save();

            $Komisaris2    = Komisaris2::find($IDPERUSAHAANFINAL);
            $Komisaris2->NAMAKOMISARIS  = $request->input('KOMISARIS_NAMA2');
            $Komisaris2->TTLKOMISARIS = $request->input('KOMISARIS_TTL2');
            $Komisaris2->TMP_LAHIR = $request->input('KOMISARIS_TMP_LAHIR2');
            $Komisaris2->KEWARGANEGARAAN = $request->input('KOMISARIS_KEWARGANEGARAAN2');
            $Komisaris2->PENDIDIKAN = $request->input('KOMISARIS_PENDIDIKAN2');
            $Komisaris2->NOTELP  = $request->input('KOMISARIS_TELP_KANTOR2');
            $Komisaris2->NOHP = $request->input('KOMISARIS_NOHP2');
            $Komisaris2->FAX = $request->input('KOMISARIS_FAX2');
            $Komisaris2->EMAIL = $request->input('KOMISARIS_EMAIL2');
            $Komisaris2->save();

            $PjbIdKeu    = PjbIdKeu::find($IDPERUSAHAANFINAL);
            $PjbIdKeu->NAMAPENANGUNGJAWAB  = $request->input('PJBK_NAMA');
            $PjbIdKeu->TTLPENANGGUNGJAWAB = $request->input('PJBK_TTL');
            $PjbIdKeu->TMP_LAHIR = $request->input('PJBK_TMP_LAHIR');
            $PjbIdKeu->KEWARGANEGARAAN = $request->input('PJBK_KEWARGANEGARAAN');
            $PjbIdKeu->PENDIDIKAN = $request->input('PJBK_PENDIDIKAN');
            $PjbIdKeu->NOTELP  = $request->input('PJBK_TELPKANTOR');
            $PjbIdKeu->NOHP = $request->input('PJBK_NOHP');
            $PjbIdKeu->FAX = $request->input('PJBK_FAX');
            $PjbIdKeu->EMAIL = $request->input('PJBK_EMAIL');
            $PjbIdKeu->save();

            $PjbIdPemberitaan    = PjbIdPemberitaan::find($IDPERUSAHAANFINAL);
            $PjbIdPemberitaan->NAMAPENANGUNGJAWAB  = $request->input('PJBP_NAMA');
            $PjbIdPemberitaan->TTLPENANGGUNGJAWAB = $request->input('PJBP_TTL');
            $PjbIdPemberitaan->TMP_LAHIR = $request->input('PJBP_TMP_LAHIR');
            $PjbIdPemberitaan->KEWARGANEGARAAN = $request->input('PJBP_KEWARGANEGARAAN');
            $PjbIdPemberitaan->PENDIDIKAN = $request->input('PJBP_PENDIDIKAN');
            $PjbIdPemberitaan->NOTELP  = $request->input('PJBP_TELPKANTOR');
            $PjbIdPemberitaan->NOHP = $request->input('PJBP_NOHP');
            $PjbIdPemberitaan->FAX = $request->input('PJBP_FAX');
            $PjbIdPemberitaan->EMAIL = $request->input('PJBP_EMAIL');
            $PjbIdPemberitaan->save();

            $PjbIdSiaran    = PjbIdSiaran::find($IDPERUSAHAANFINAL);
            $PjbIdSiaran->NAMAPENANGUNGJAWAB  = $request->input('PJBS_NAMA');
            $PjbIdSiaran->TTLPENANGGUNGJAWAB = $request->input('PJBS_TTL');
            $PjbIdSiaran->TMP_LAHIR = $request->input('PJBS_TMP_LAHIR');
            $PjbIdSiaran->KEWARGANEGARAAN = $request->input('PJBS_KEWARGANEGARAAN');
            $PjbIdSiaran->PENDIDIKAN = $request->input('PJBS_PENDIDIKAN');
            $PjbIdSiaran->NOTELP  = $request->input('PJBS_TELPKANTOR');
            $PjbIdSiaran->NOHP = $request->input('PJBS_NOHP');
            $PjbIdSiaran->FAX = $request->input('PJBS_FAX');
            $PjbIdSiaran->EMAIL = $request->input('PJBS_EMAIL');
            $PjbIdSiaran->save();

            $PjbIdTeknik    = PjbIdTeknik::find($IDPERUSAHAANFINAL);
            $PjbIdTeknik->NAMAPENANGUNGJAWAB  = $request->input('PJBT_NAMA');
            $PjbIdTeknik->TTLPENANGGUNGJAWAB = $request->input('PJBT_TTL');
            $PjbIdTeknik->TMP_LAHIR = $request->input('PJBT_TMP_LAHIR');
            $PjbIdTeknik->KEWARGANEGARAAN = $request->input('PJBT_KEWARGANEGARAAN');
            $PjbIdTeknik->PENDIDIKAN = $request->input('PJBT_PENDIDIKAN');
            $PjbIdTeknik->NOTELP  = $request->input('PJBT_TELPKANTOR');
            $PjbIdTeknik->NOHP = $request->input('PJBT_NOHP');
            $PjbIdTeknik->FAX = $request->input('PJBT_FAX');
            $PjbIdTeknik->EMAIL = $request->input('PJBT_EMAIL');
            $PjbIdTeknik->save();

            $PjbIdUsaha    = PjbIdUsaha::find($IDPERUSAHAANFINAL);
            $PjbIdUsaha->NAMAPENANGUNGJAWAB  = $request->input('PJBU_NAMA');
            $PjbIdUsaha->TTLPENANGGUNGJAWAB = $request->input('PJBU_TTL');
            $PjbIdUsaha->TMP_LAHIR = $request->input('PJBU_TMP_LAHIR');
            $PjbIdUsaha->KEWARGANEGARAAN = $request->input('PJBU_KEWARGANEGARAAN');
            $PjbIdUsaha->PENDIDIKAN = $request->input('PJBU_PENDIDIKAN');
            $PjbIdUsaha->NOTELP  = $request->input('PJBU_TELPKANTOR');
            $PjbIdUsaha->NOHP = $request->input('PJBU_NOHP');
            $PjbIdUsaha->FAX = $request->input('PJBU_FAX');
            $PjbIdUsaha->EMAIL = $request->input('PJBU_EMAIL');
            $PjbIdUsaha->save();

            $PendirianLppLokal    = pendirian_lpplokal::find($IDPERUSAHAANFINAL);
            $PendirianLppLokal->NO_LPP  = $request->input('PLPP_LOKAL_NO');
            $PendirianLppLokal->TGL = $request->input('PLPP_LOKAL_TGL');
            $PendirianLppLokal->NMPENANGGUNGJAWAB = $request->input('PLPP_LOKAL_NM_PJ');
            $PendirianLppLokal->save();

            $PengAktaPendirian    = peng_aktapendirian::find($IDPERUSAHAANFINAL);
            $PengAktaPendirian->NOAKTA  = $request->input('NOAKTA_PAP');
            $PengAktaPendirian->TGL = $request->input('TANGGALPENGESAHAN_PAP');
            $PengAktaPendirian->NMINSTANSI = $request->input('NAMAINSTANSI_PAP');
            $PengAktaPendirian->save();

            $PengAktaPerubahan    = peng_aktaperubahan::find($IDPERUSAHAANFINAL);
            $PengAktaPerubahan->NOAKTA  = $request->input('NOAKTA_PAPT');
            $PengAktaPerubahan->TGL = $request->input('TANGGALPENGESAHAN_PAPT');
            $PengAktaPerubahan->NMINSTANSI = $request->input('NAMAINSTANSI_PAPT');
            $PengAktaPerubahan->save();

            $SuratKetDomisili    = SuratKetDomisili::find($IDPERUSAHAANFINAL);
            $SuratKetDomisili->NOAKTA  = $request->input('NOSURAT_LPP');
            $SuratKetDomisili->TGL = $request->input('TANGGALSURAT_LPP');
            $SuratKetDomisili->NMINSTANSI = $request->input('NAMAINSTANSI_LPP');
            $SuratKetDomisili->save();

            $SistemModulasi    = SistemModulasi::find($IDPERUSAHAANFINAL);
            $SistemModulasi->SISTEM_MODULASI  = $request->input('SISMOD');
            // // $SistemModulasi->TGL = $request->input('TGL');
            // // $SistemModulasi->NMINSTANSI = $request->input('NMINSTANSI');
            $SistemModulasi->save();

            $StasiunDistribusi    = StasiunDistribusi::find($IDPERUSAHAANFINAL);
            $StasiunDistribusi->MULAI_BEROPERASI  = $request->input('MULAI_BEROPERASISTASIUNDISTRIBUSI');
            $StasiunDistribusi->MODA_SUARA = $request->input('MPSSTASIUNDISTRIBUSI');
            $StasiunDistribusi->SISTEM_HUBUNGAN = $request->input('SISTEM_HUBUNGANSTASIUNDISTRIBUSI');
            $StasiunDistribusi->SDP = $request->input('SDPSTASIUNDISTRIBUSI');
            $StasiunDistribusi->save();

            $StasiunPemancar    = StasiunPemancar::find($IDPERUSAHAANFINAL);
            $StasiunPemancar->NMSTASIUNPEMANCAR  = $request->input('NAMA_SP');
            $StasiunPemancar->TGLOPERASI = $request->input('TGLOPERASI_SP');
            $StasiunPemancar->SISTEM_HUBUNGAN = $request->input('SISTEM_HUBUNGAN_SP');
            $StasiunPemancar->save();
            //
            // $SpekSatelit    = SpekSatelit::find($IDPERUSAHAANFINAL);
            // $SpekSatelit->NAMA  = $request->input('NAMA');
            // $SpekSatelit->NEGARA = $request->input('NEGARA');
            // $SpekSatelit->RANGE = $request->input('RANGE');
            // $SpekSatelit->LEBARPITA = $request->input('LEBARPITA');
            // $SpekSatelit->KOORDINAT = $request->input('KOORDINAT');
            // $SpekSatelit->save();
          //  // $SpekSatelit->update($request->all());
           $SpekSatelit    = SpekSatelit::find($IDPERUSAHAANFINAL);
           $SpekSatelit->NAMA  = $request->input('NAMA_SS');
           $SpekSatelit->NEGARA = $request->input('NEGARA_SS');
           $SpekSatelit->RANGE = $request->input('RANGE_SS');
           $SpekSatelit->LEBARPITA = $request->input('LEBARPITA_SS');
           $SpekSatelit->KOORDINAT = $request->input('KOORDINAT_SS');
           $SpekSatelit->save();
            //
            $SatelitPenyiaran    = SatelitPenyiaran::find($IDPERUSAHAANFINAL);
            $SatelitPenyiaran->NAMA  = $request->input('NAMA_SATPEN');
            $SatelitPenyiaran->NEGARA = $request->input('NEGARA_SATPEN');
            $SatelitPenyiaran->FREKDOWN = $request->input('FREAKDOWN_SATPEN');
            $SatelitPenyiaran->LEBARPITA = $request->input('LEBARPITA_SATPEN');
            $SatelitPenyiaran->KOORDINAT = $request->input('KOORDINAT_SATPEN');
            $SatelitPenyiaran->save();

            $SpekPerlRadio    = SpekPerlRadio::find($IDPERUSAHAANFINAL);
            $SpekPerlRadio->MEREK  = $request->input('MEREK_SPR');
            $SpekPerlRadio->TIPE = $request->input('TIPE_SPR');
            $SpekPerlRadio->NOSERI = $request->input('NOSERI_SPR');
            $SpekPerlRadio->BUATAN = $request->input('JENIS_SPR');
            $SpekPerlRadio->TAHUN = $request->input('tahun_SPR');
            $SpekPerlRadio->FREK = $request->input('FREK_SPR');
            $SpekPerlRadio->LEBARFREK = $request->input('LEBARFREK_SPR');
            $SpekPerlRadio->save();

            $Transponder    = Transponder::find($IDPERUSAHAANFINAL);
            $Transponder->JMLTRANSPONDER  = $request->input('JMLTRANSPONDER');
            $Transponder->BWTRANSPONDERA = $request->input('BWTRANSPONDERA');
            $Transponder->BWTRANSPONDERZ = $request->input('BWTRANSPONDERZ');
            $Transponder->JMLSALURAN = $request->input('JMLSALURAN');
            $Transponder->FREKDOWNA = $request->input('FREKDOWNA');
            $Transponder->FREKDOWNZ = $request->input('FREKDOWNZ');
            $Transponder->FREKUPA = $request->input('FREKUPA');
            $Transponder->FREKUPZ = $request->input('FREKUPZ');
            $Transponder->save();

            $PeralatanPemancar    = PeralatanPemancar::find($IDPERUSAHAANFINAL);
            $PeralatanPemancar->MEREK  = $request->input('MEREK_PP');
            $PeralatanPemancar->TIPE = $request->input('TIPE_PP');
            $PeralatanPemancar->NOSERI = $request->input('NOSERI_PP');
            $PeralatanPemancar->BUATAN = $request->input('BUATAN_PP');
            $PeralatanPemancar->TAHUN = $request->input('tahun_PP');
            $PeralatanPemancar->DAYAPEMANCARMAKS = $request->input('DAYAOEMANCARMAX_PP');
            $PeralatanPemancar->DAYAPEMANCARTERPASANG = $request->input('DAYAPEMANCARTERPASANG_PP');
            $PeralatanPemancar->save();

            $Feeder    = Feeder::find($IDPERUSAHAANFINAL);
            $Feeder->JENIS  = $request->input('JENIS_FEEDER');
            $Feeder->MEREK = $request->input('MEREK_FEEDER');
            $Feeder->TIPEUKURAN = $request->input('TIPEUKURAN_UKURAN');
            $Feeder->PANJANGKABEL = $request->input('PANJANGKABEL_FEEDER');
            $Feeder->LOSSKABEL = $request->input('LOSSKABEL_FEEDER');
            $Feeder->TOTALLOSFEEDER = $request->input('TOTALLOSFEEDER');
            $Feeder->save();

            $FormatSiaran    = FormatSiaran::find($IDPERUSAHAANFINAL);
            $FormatSiaran->FORMATSIARAN  = $request->input('FORMATSIARAN_FS');
            $FormatSiaran->LAINNYA = $request->input('LAINNYA_FS');
            $FormatSiaran->save();

            $SurveyJk    = SurveyJk::find($IDPERUSAHAANFINAL);
            $SurveyJk->JENISSURVEY  = $request->input('JENISSURVEY_JK');
            $SurveyJk->PRIA = $request->input('PRIA_JK');
            $SurveyJk->WANITA = $request->input('WANITA_JK');
            $SurveyJk->save();
          //   // ditanyakan lebih lanjut
          //   // // $SurveyUsia    = SurveyUsia::find($IDPERUSAHAANFINAL);
          //   // // $SurveyUsia->JENISSURVEY  = $request->input('JENISSURVEY');
          //   // // $SurveyUsia->BAWAH15 = $request->input('BAWAH15');
          //   // // $SurveyUsia->16SD19 = $request->input('16SD19');
          //   // // $SurveyUsia->20SD24  = $request->input('20SD24');
          //   // // $SurveyUsia->25SD29 = $request->input('25SD29');
          //   // // $SurveyUsia->30SD34 = $request->input('30SD34');
          //   // // $SurveyUsia->35SD39  = $request->input('35SD39');
          //   // // $SurveyUsia->40SD50 = $request->input('40SD50');
          //   // // $SurveyUsia->ATAS50 = $request->input('ATAS50');
          //   // // $SurveyUsia->save();
          //   //
            $SurveyPekerjaan    = SurveyPekerjaan::find($IDPERUSAHAANFINAL);
            $SurveyPekerjaan->JENISSURVEY  = $request->input('JENISSURVEY_SPK');
            $SurveyPekerjaan->PNS = $request->input('PNS_SPK');
            $SurveyPekerjaan->PEGAWAISWASTA = $request->input('PEGAWAISWASTA_SPK');
            $SurveyPekerjaan->WIRASWASTA  = $request->input('WIRASWASTA_SPK');
            $SurveyPekerjaan->PENSIUNAN = $request->input('PENSIUNAN_SPK');
            $SurveyPekerjaan->PELAJAR = $request->input('PELAJAR_SPK');
            $SurveyPekerjaan->MAHASISWA  = $request->input('MAHASISWA_SPK');
            $SurveyPekerjaan->IRT = $request->input('IRT_SPK');
            $SurveyPekerjaan->LAINNYA = $request->input('LAINNYA_SPK');
            $SurveyPekerjaan->TIDAKBEKERJA = $request->input('TIDAKBEKERJA_SPK');
            $SurveyPekerjaan->save();

            $SurveyPendidikan    = SurveyPendidikan::find($IDPERUSAHAANFINAL);
            $SurveyPendidikan->JENISSURVEY  = $request->input('JENISSURVEY_SPT');
            $SurveyPendidikan->TIDAKSD = $request->input('TIDAKSD_SPT');
            $SurveyPendidikan->SD = $request->input('SD_SPT');
            $SurveyPendidikan->SLTP  = $request->input('SLTP_SPT');
            $SurveyPendidikan->SLTA = $request->input('SLTA_SPT');
            $SurveyPendidikan->AKADEMI = $request->input('AKADEMI_SPT');
            $SurveyPendidikan->PT  = $request->input('PT_SPT');
            $SurveyPendidikan->save();

            $SurveyEkonomi    = SurveyEkonomi::find($IDPERUSAHAANFINAL);
            $SurveyEkonomi->JENISSURVEY  = $request->input('JENISSURVEY_E');
            $SurveyEkonomi->ATAS3JT = $request->input('ATAS3JT');
            $SurveyEkonomi->{'2JTSD3JT'} = $request->input('2JTSD3JT');
            $SurveyEkonomi->{'1JTSD2JT'}  = $request->input('1JTSD2JT');
            $SurveyEkonomi->{'7SD1JT'} = $request->input('7SD1JT');
            $SurveyEkonomi->{'5SD7'} = $request->input('5SD7');
            $SurveyEkonomi->BAWAH5  = $request->input('BAWAH5');
            $SurveyEkonomi->save();

            $SumberMateri    = SumberMateri::find($IDPERUSAHAANFINAL);
            $SumberMateri->SENDIRI  = $request->input('SENDIRI_SMAS');
            $SumberMateri->AKUISISI = $request->input('AKUISISI_SMAS');
            $SumberMateri->KERJASAMA = $request->input('KERJASAMA_SMAS');
            $SumberMateri->save();

            $WaktusiaranLpb    = WaktusiaranLpb::find($IDPERUSAHAANFINAL);
            $WaktusiaranLpb->KANAL24JAM  = $request->input('KANAL24JAM');
            $WaktusiaranLpb->KANALKURANG24JAM = $request->input('KANALKURANG24JAM');
            $WaktusiaranLpb->save();

            $PersentaseMataSiaran    = PersentaseMataSiaran::find($IDPERUSAHAANFINAL);
            $PersentaseMataSiaran->UMUM  = $request->input('ms_lpb_umum');
            $PersentaseMataSiaran->BERITA = $request->input('ms_lpb_berita');
            $PersentaseMataSiaran->PENDIDIKAN = $request->input('ms_lpb_pendidikan');
            $PersentaseMataSiaran->AGAMA  = $request->input('ms_lpb_agama');
            $PersentaseMataSiaran->OLAHRAGA = $request->input('ms_lpb_olahraga');
            $PersentaseMataSiaran->FILM = $request->input('ms_lpb_film');
            $PersentaseMataSiaran->MUSIK  = $request->input('ms_lpb_musik');
            $PersentaseMataSiaran->LAINNYA  = $request->input('ms_lpb_lainnya');
            $PersentaseMataSiaran->save();

            $PersentaseSiaran    = PersentaseSiaran::find($IDPERUSAHAANFINAL);
            $PersentaseSiaran->BERITA  = $request->input('PS_BERITA');
            $PersentaseSiaran->INFORMASI = $request->input('PS_INFORMASI');
            $PersentaseSiaran->PENDIDIKAN = $request->input('PS_PENDIDIKAN');
            $PersentaseSiaran->AGAMA  = $request->input('PS_AGAMA');
            $PersentaseSiaran->OLAHRAGA = $request->input('PS_OLAHRAGA');
            $PersentaseSiaran->HIBURAN = $request->input('PS_HIBURAN');
            $PersentaseSiaran->IKLAN  = $request->input('PS_IKLAN');
            $PersentaseSiaran->LAYANANMASY  = $request->input('PS_LAYANANMASY');
            $PersentaseSiaran->save();

            $PersentaseSiaranMusik    = PersentaseSiaranMusik::find($IDPERUSAHAANFINAL);
            $PersentaseSiaranMusik->INDONESIA  = $request->input('PSM_INDONESIA');
            $PersentaseSiaranMusik->DANGDUT = $request->input('PSM_DANGDUT');
            $PersentaseSiaranMusik->BARAT = $request->input('PSM_BARAT');
            $PersentaseSiaranMusik->DAERAH  = $request->input('PSM_DAERAH');
            $PersentaseSiaranMusik->KERONCONG = $request->input('PSM_KERONCONG');
            $PersentaseSiaranMusik->LAINNYA = $request->input('PSM_LAINNYA');
            $PersentaseSiaranMusik->save();

          //  // persentasesiaranlpb di tanyakan lebih lanjut (tidak jadi dikerjakan)
          //  // $PersentaseSiaranLpb    = PersentaseSiaranLpb::find($IDPERUSAHAANFINAL);
          //  // $PersentaseSiaranLpb->LOKAL  = $request->input('LOKAL');
          //  // $PersentaseSiaranLpb->LINGKUNGANSENDIRI = $request->input('LINGKUNGANSENDIRI');
          //  // $PersentaseSiaranLpb->LUARPERUSAHAAN = $request->input('LUARPERUSAHAAN');
          //  // $PersentaseSiaranLpb->FREETOAIR  = $request->input('FREETOAIR');
          //  // $PersentaseSiaranLpb->ASING = $request->input('ASING');
          //  // $PersentaseSiaranLpb->JMLPELANGGAN = $request->input('JMLPELANGGAN');
          //  // $PersentaseSiaranLpb->save();

            $MateriSiaran    = MateriSiaran::find($IDPERUSAHAANFINAL);
            $MateriSiaran->LOKAL  = $request->input('LOKAL_PMS');
            $MateriSiaran->ASING = $request->input('ASING_PMS');
            $MateriSiaran->save();

            // // waktu siaran di tanyakan lebih lanjut
          $WaktuSiaran    = WaktuSiaran::find($IDPERUSAHAANFINAL);
          $WaktuSiaran->HARIKERJAAWAL  = $request->input('HARIKERJAAWAL');
          $WaktuSiaran->HARILIBURAWAL = $request->input('HARILIBURAWAL');
          $WaktuSiaran->HARIKERJAAKHIR  = $request->input('HARIKERJAAKHIR');
          $WaktuSiaran->HARILIBURAKHIR = $request->input('HARILIBURAKHIR');
          $WaktuSiaran->save();

           // pemegang saham di tanyakan lebih lanjut
           // $KomposisiPemegangSaham    = KomposisiPemegangSaham::find($IDPERUSAHAANFINAL);
           // $KomposisiPemegangSaham->WNIS  = $request->input('WNI');
           // $KomposisiPemegangSaham->WNA = $request->input('WNA');
           // $KomposisiPemegangSaham->save();

          // penyelenggara  di tanyakan lebih lanjut
           $Penyelenggara    = Penyelenggara::find($IDPERUSAHAANFINAL);
           $Penyelenggara->IDPERUSAHAANFINAL  = $request->input('IDPERUSAHAANFINAL');
           $Penyelenggara->NMPERUSAHAAN = $request->input('NMPERUSAHAANPENYELENGGARA');
           $Penyelenggara->NMUDARA = $request->input('NMUDARA');
           $Penyelenggara->save();

            // $tbl_ipp_prinsip    = tbl_ipp_prinsip::find($IDPERUSAHAANFINAL);
            // $tbl_ipp_prinsip->id_perusahaan  = $request->input('id_perusahaan_ipp_prinsip');
            // $tbl_ipp_prinsip->no_ipp_prinsip = $request->input('no_ipp_prinsip');
            // $tbl_ipp_prinsip->tgl_ipp_prinsip = $request->input('tgl_ipp_prinsip');
            // $tbl_ipp_prinsip->tgl_perpanjangan = $request->input('tgl_perpanjangan_ipp_prinsip');
            // $tbl_ipp_prinsip->save();

            //ditanyakan lebih lanjut
        $tbl_ipp_tetap    = tbl_ipp_tetap::find($IDPERUSAHAANFINAL);
          $tbl_ipp_tetap->id_perusahaan  = $request->input('id_perusahaan_ipp_tetap');
          $tbl_ipp_tetap->no_ipp_tetap = $request->input('NOSURAT_IPP_TETAP');
          $tbl_ipp_tetap->tgl_ipp_tetap = $request->input('TANGGALSURAT_IPP_TETAP');
          $tbl_ipp_tetap->tgl_penetapan = $request->input('TANGGALSURAT_IPP_TETAP');
          $tbl_ipp_tetap->tgl_perpanjangan = $request->input('TANGGALSURAT_IPP_TETAP_BERAKHIR');
          $tbl_ipp_tetap->save();

    $SumberMateriSiaranLpb = SumberMateriSiaranLpb::find($IDPERUSAHAANFINAL);

    if(isset($SumberMateriSiaranLpb) && $SumberMateriSiaranLpb->delete($IDPERUSAHAANFINAL)){
      //$SumberMateriSiaran_Lpb->IDPERUSAHAANFINAL = $request->input('IDPERUSAHAANFINAL');
      // $SumberMateriSiaran_Lpb->NAMAPERUSAHAAN = $request->input('NAMAPERUSAHAAN');
      // $SumberMateriSiaran_Lpb->NAMAKANAL = $request->input('NAMAKANAL');
      // $data =array();
      // $SumberMateriSiaran_Lpb = array();
      // for($i=0; $i<count($request->input()); $i++){
      //   $SumberMateriSiaran_Lpb[$i] = array(
      //      'NAMAPERUSAHAAN' => $namakanal_sumber[$i],
      //      'NAMAKANAL' => $dalamnegeri_sumber[$i],
      //      );
      //
      // }
      $namakanal_sumber = $request->input('NAMAKANAL_SUMBER');
      $dalamnegeri_sumber = $request->input('DALAMNEGERI_SUMBER');
      $SumberMateriSiaran_Lpb = array();

      for($i=0; $i<count($dalamnegeri_sumber); $i++){
        // dd($request->input());
        $SumberMateriSiaran_Lpb[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAPERUSAHAAN' => $namakanal_sumber[$i],
          'NAMAKANAL' => $dalamnegeri_sumber[$i],
        );

      }
      // $i=0;
      // foreach ( $request->input() as $value) {
      //   // dd( $value['NAMAKANAL']);
      //   $SumberMateriSiaran_Lpb[$i] = array(
      //     'IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL,
      //     'NAMAPERUSAHAAN' => $value['NAMAKANAL_SUMBER'],
      //     'NAMAKANAL' => $value['DALAMNEGERI_SUMBER']);
      //     $i++;
      //   }

      // dd($SumberMateriSiaran_Lpb);
      // $SumberMateriSiaran_Lpb->save();
      // DB::table('SumberMateriSiaranLpb')->insert($SumberMateriSiaran_Lpb);

      SumberMateriSiaranLpb::insert($SumberMateriSiaran_Lpb);

    }
    else if($SumberMateriSiaranLpb == null){
      $namakanal_sumber = $request->input('NAMAKANAL_SUMBER');
      $dalamnegeri_sumber = $request->input('DALAMNEGERI_SUMBER');
      $SumberMateriSiaran_Lpbs = array();
      for($i=0; $i<count($dalamnegeri_sumber); $i++){
        // dd($request->input());
        $SumberMateriSiaran_Lpbs[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAPERUSAHAAN' => $namakanal_sumber[$i],
          'NAMAKANAL' => $dalamnegeri_sumber[$i],
        );

      }
      // $i=0;
      // foreach ( $request->input() as $values) {
      //   // dd( $value['NAMAKANAL']);
      //   $SumberMateriSiaran_Lpbs[$i] = array(
      //     'IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL,
      //     'NAMAPERUSAHAAN' => $values['NAMAKANAL_SUMBER'],
      //     'NAMAKANAL' => $values['DALAMNEGERI_SUMBER']);
      //     $i++;
      //   }
      // dd($request->input());
      // dd($SumberMateriSiaran_Lpb);
      // $SumberMateriSiaran_Lpb->save();
      // dd($SumberMateriSiaran_Lpbs);
      // DB::table('SumberMateriSiaranLpb')->insert($SumberMateriSiaran_Lpbs);
      // dd($SumberMateriSiaran_Lpbs);
      SumberMateriSiaranLpb::insert($SumberMateriSiaran_Lpbs);

    }


    $IklanKomersial = IklanKomersial::find($IDPERUSAHAANFINAL);

    if(isset($IklanKomersial) && $IklanKomersial->delete($IDPERUSAHAANFINAL)){

      $namakanal_iklan = $request->input('NAMAKANAL_IKLAN');
      $persentase_iklan = $request->input('PERSENTASE_IKLAN');
      $Iklan_Komersial = array();

      for($i=0; $i<count($namakanal_iklan); $i++){
        // dd($request->input());
        $Iklan_Komersial[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_iklan[$i],
          'PERSENTASE' => $persentase_iklan[$i],
        );

      }


      IklanKomersial::insert($Iklan_Komersial);

    }
    else if($IklanKomersial == null){
      $namakanal_iklan = $request->input('NAMAKANAL_IKLAN');
      $persentase_iklan = $request->input('PERSENTASE_IKLAN');
      $Iklan_Komersials = array();
      for($i=0; $i<count($namakanal_iklan); $i++){
        // dd($request->input());
        $Iklan_Komersials[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_iklan[$i],
          'PERSENTASE' => $persentase_iklan[$i],
        );

      }
      IklanKomersial::insert($Iklan_Komersials);

    }

    $TotalKanalUmum = TotalKanalUmum::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalUmum) && $TotalKanalUmum->delete($IDPERUSAHAANFINAL)){
      $namakanal_umum = $request->input('NAMAKANAL_UMUM');
      $dalamnegeri_umun = $request->input('DALAMNEGERI_UMUM');
      $TotalKanal_Umum = array();

      for($i=0; $i<count($namakanal_umum); $i++){
        // dd($request->input());
        $TotalKanal_Umum[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_umum[$i],
          'ASALKANAL' => $dalamnegeri_umun[$i],
        );

      }
      TotalKanalUmum::insert($TotalKanal_Umum);

    }
    else if($TotalKanalUmum == null){
      $namakanal_umum = $request->input('NAMAKANAL_UMUM');
      $dalamnegeri_umun = $request->input('DALAMNEGERI_UMUM');
      $TotalKanal_Umums = array();
      for($i=0; $i<count($namakanal_umum); $i++){
        // dd($request->input());
        $TotalKanal_Umums[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_umum[$i],
          'ASALKANAL' => $dalamnegeri_umun[$i],
        );

      }
      TotalKanalUmum::insert($TotalKanal_Umums);

    }

    $TotalKanalBerita = TotalKanalBerita::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalBerita) && $TotalKanalBerita->delete($IDPERUSAHAANFINAL)){
      $namakanal_berita = $request->input('NAMAKANAL_BERITA');
      $dalamnegeri_berita = $request->input('DALAMNEGERI_BERITA');
      $TotalKanal_Berita = array();

      for($i=0; $i<count($namakanal_berita); $i++){
        // dd($request->input());
        $TotalKanal_Berita[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_berita[$i],
          'ASALKANAL' => $dalamnegeri_berita[$i],
        );

      }
      TotalKanalBerita::insert($TotalKanal_Berita);

    }
    else if($TotalKanalBerita == null){
      $namakanal_berita = $request->input('NAMAKANAL_BERITA');
      $dalamnegeri_berita = $request->input('DALAMNEGERI_BERITA');
      $TotalKanal_Beritas = array();
      for($i=0; $i<count($namakanal_berita); $i++){
        // dd($request->input());
        $TotalKanal_Beritas[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_berita[$i],
          'ASALKANAL' => $dalamnegeri_berita[$i],
        );

      }
      TotalKanalBerita::insert($TotalKanal_Beritas);

    }

    $TotalKanalMusik = TotalKanalMusik::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalMusik) && $TotalKanalMusik->delete($IDPERUSAHAANFINAL)){
      $namakanal_musik = $request->input('NAMAKANAL_MUSIK');
      $dalamnegeri_musik = $request->input('DALAMNEGERI_MUSIK');
      $TotalKanal_Musik = array();

      for($i=0; $i<count($namakanal_musik); $i++){
        // dd($request->input());
        $TotalKanal_Musik[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_musik[$i],
          'ASALKANAL' => $dalamnegeri_musik[$i],
        );

      }
      TotalKanalMusik::insert($TotalKanal_Musik);

    }
    else if($TotalKanalMusik == null){
      $namakanal_musik = $request->input('NAMAKANAL_MUSIK');
      $dalamnegeri_musik = $request->input('DALAMNEGERI_MUSIK');
      $TotalKanal_Musiks = array();
      for($i=0; $i<count($namakanal_musik); $i++){
        // dd($request->input());
        $TotalKanal_Musiks[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_musik[$i],
          'ASALKANAL' => $dalamnegeri_musik[$i],
        );

      }
      TotalKanalMusik::insert($TotalKanal_Musiks);

    }

    $TotalKanalOlahraga = TotalKanalOlahraga::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalOlahraga) && $TotalKanalOlahraga->delete($IDPERUSAHAANFINAL)){
      $namakanal_olahraga = $request->input('NAMAKANAL_OLAHRAGA');
      $dalamnegeri_olahraga = $request->input('DALAMNEGERI_OLAHRAGA');
      $TotalKanal_Olahraga = array();

      for($i=0; $i<count($namakanal_olahraga); $i++){
        // dd($request->input());
        $TotalKanal_Olahraga[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_olahraga[$i],
          'ASALKANAL' => $dalamnegeri_olahraga[$i],
        );

      }
      TotalKanalOlahraga::insert($TotalKanal_Olahraga);

    }
    else if($TotalKanalOlahraga == null){
      $namakanal_olahraga = $request->input('NAMAKANAL_OLAHRAGA');
      $dalamnegeri_olahraga = $request->input('DALAMNEGERI_OLAHRAGA');
      $TotalKanal_Olahragas = array();
      for($i=0; $i<count($namakanal_olahraga); $i++){
        // dd($request->input());
        $TotalKanal_Olahragas[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_olahraga[$i],
          'ASALKANAL' => $dalamnegeri_olahraga[$i],
        );

      }
      TotalKanalOlahraga::insert($TotalKanal_Olahragas);

    }

    $TotalKanalPendidikan = TotalKanalPendidikan::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalPendidikan) && $TotalKanalPendidikan->delete($IDPERUSAHAANFINAL)){
      $namakanal_pendidikan = $request->input('NAMAKANAL_PENDIDIKAN');
      $dalamnegeri_pendidikan = $request->input('DALAMNEGERI_PENDIDIKAN');
      $TotalKanal_Pendidikan = array();

      for($i=0; $i<count($namakanal_pendidikan); $i++){
        // dd($request->input());
        $TotalKanal_Pendidikan[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_pendidikan[$i],
          'ASALKANAL' => $dalamnegeri_pendidikan[$i],
        );

      }
      TotalKanalPendidikan::insert($TotalKanal_Pendidikan);

    }
    else if($TotalKanalPendidikan == null){
      $namakanal_pendidikan = $request->input('NAMAKANAL_PENDIDIKAN');
      $dalamnegeri_pendidikan = $request->input('DALAMNEGERI_PENDIDIKAN');
      $TotalKanal_Pendidikans = array();
      for($i=0; $i<count($namakanal_pendidikan); $i++){
        // dd($request->input());
        $TotalKanal_Pendidikans[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_pendidikan[$i],
          'ASALKANAL' => $dalamnegeri_pendidikan[$i],
        );

      }
      TotalKanalPendidikan::insert($TotalKanal_Pendidikans);

    }

    $TotalKanalDakwah = TotalKanalDakwah::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalDakwah) && $TotalKanalDakwah->delete($IDPERUSAHAANFINAL)){
      $namakanal_dakwah = $request->input('NAMAKANAL_DAKWAH');
      $dalamnegeri_dakwah = $request->input('DALAMNEGERI_DAKWAH');
      $TotalKanal_Dakwah = array();

      for($i=0; $i<count($namakanal_dakwah); $i++){
        // dd($request->input());
        $TotalKanal_Dakwah[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_dakwah[$i],
          'ASALKANAL' => $dalamnegeri_dakwah[$i],
        );

      }
      TotalKanalDakwah::insert($TotalKanal_Dakwah);

    }
    else if($TotalKanalDakwah == null){
      $namakanal_dakwah = $request->input('NAMAKANAL_DAKWAH');
      $dalamnegeri_dakwah = $request->input('DALAMNEGERI_DAKWAH');
      $TotalKanal_Dakwahs = array();
      for($i=0; $i<count($namakanal_dakwah); $i++){
        // dd($request->input());
        $TotalKanal_Dakwahs[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_dakwah[$i],
          'ASALKANAL' => $dalamnegeri_dakwah[$i],
        );

      }
      TotalKanalDakwah::insert($TotalKanal_Dakwahs);

    }

    $TotalKanalFilm = TotalKanalFilm::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalFilm) && $TotalKanalFilm->delete($IDPERUSAHAANFINAL)){
      $namakanal_film = $request->input('NAMAKANAL_FILM');
      $dalamnegeri_film = $request->input('DALAMNEGERI_FILM');
      $TotalKanal_Film = array();

      for($i=0; $i<count($namakanal_film); $i++){
        // dd($request->input());
        $TotalKanal_Film[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_film[$i],
          'ASALKANAL' => $dalamnegeri_film[$i],
        );

      }
      TotalKanalFilm::insert($TotalKanal_Film);

    }
    else if($TotalKanalFilm == null){
      $namakanal_film = $request->input('NAMAKANAL_FILM');
      $dalamnegeri_film = $request->input('DALAMNEGERI_FILM');
      $TotalKanal_Films = array();
      for($i=0; $i<count($namakanal_film); $i++){
        // dd($request->input());
        $TotalKanal_Films[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_film[$i],
          'ASALKANAL' => $dalamnegeri_film[$i],
        );

      }
      TotalKanalFilm::insert($TotalKanal_Films);

    }

    $TotalKanalLainnya = TotalKanalLainnya::find($IDPERUSAHAANFINAL);

    if(isset($TotalKanalLainnya) && $TotalKanalLainnya->delete($IDPERUSAHAANFINAL)){
      $namakanal_lainnya = $request->input('NAMAKANAL_LAINNYA');
      $dalamnegeri_lainnya = $request->input('DALAMNEGERI_LAINNYA');
      $TotalKanal_Lainnya = array();

      for($i=0; $i<count($namakanal_lainnya); $i++){
        // dd($request->input());
        $TotalKanal_Lainnya[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_lainnya[$i],
          'ASALKANAL' => $dalamnegeri_lainnya[$i],
        );

      }
      TotalKanalLainnya::insert($TotalKanal_Lainnya);

    }
    else if($TotalKanalLainnya == null){
      $namakanal_lainnya = $request->input('NAMAKANAL_LAINNYA');
      $dalamnegeri_lainnya = $request->input('DALAMNEGERI_LAINNYA');
      $TotalKanal_Lainnyas = array();
      for($i=0; $i<count($namakanal_lainnya); $i++){
        // dd($request->input());
        $TotalKanal_Lainnyas[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAKANAL' => $namakanal_lainnya[$i],
          'ASALKANAL' => $dalamnegeri_lainnya[$i],
        );

      }
      TotalKanalLainnya::insert($TotalKanal_Lainnyas);

    }

    $PemusatanKepemilikanSilang = PemusatanKepemilikanSilang::find($IDPERUSAHAANFINAL);

    if(isset($PemusatanKepemilikanSilang) && $PemusatanKepemilikanSilang->delete($IDPERUSAHAANFINAL)){
      $namapemegangsaham = $request->input('NAMAPEMEGANGSAHAM');
      $mediacetak = $request->input('MEDIACETAK');
      $persentasemc = $request->input('PERSENTASEMC');
      $lpsradio = $request->input('LPSRADIO');
      $persentaselpsr = $request->input('PERSENTASELPSR');
      $lpstelevisi = $request->input('LPSTELEVISI');
      $persentaselpstv = $request->input('PERSENTASELPSTV');
      $lpb = $request->input('LPB');
      $persentaselpb = $request->input('PERSENTASELPB');
      $Pemusatan_KepemilikanSilang = array();

      for($i=0; $i<count($namapemegangsaham); $i++){
        // dd($request->input());
        $Pemusatan_KepemilikanSilang[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAPEMEGANGSAHAM' => $namapemegangsaham[$i],
          'MEDIACETAK' => $mediacetak[$i],
          'PERSENTASEMC'=>$persentasemc[$i],
          'LPSRADIO' => $lpsradio[$i],
          'PERSENTASELPSR' => $persentaselpsr[$i],
          'LPSTELEVISI'=>$lpstelevisi[$i],
          'PERSENTASELPSTV' => $persentaselpstv[$i],
          'LPB' => $lpb[$i],
          'PERSENTASELPB' => $persentaselpb[$i],
        );

      }
      PemusatanKepemilikanSilang::insert($Pemusatan_KepemilikanSilang);

    }
    else if($PemusatanKepemilikanSilang == null){
      $namapemegangsaham = $request->input('NAMAPEMEGANGSAHAM');
      $mediacetak = $request->input('MEDIACETAK');
      $persentasemc = $request->input('PERSENTASEMC');
      $lpsradio = $request->input('LPSRADIO');
      $persentaselpsr = $request->input('PERSENTASELPSR');
      $lpstelevisi = $request->input('LPSTELEVISI');
      $persentaselpstv = $request->input('PERSENTASELPSTV');
      $lpb = $request->input('LPB');
      $persentaselpb = $request->input('PERSENTASELPB');
      $Pemusatan_KepemilikanSilangs = array();
      for($i=0; $i<count($namapemegangsaham); $i++){
        // dd($request->input());
        $Pemusatan_KepemilikanSilangs[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMAPEMEGANGSAHAM' => $namapemegangsaham[$i],
          'MEDIACETAK' => $mediacetak[$i],
          'PERSENTASEMC'=>$persentasemc[$i],
          'LPSRADIO' => $lpsradio[$i],
          'PERSENTASELPSR' => $persentaselpsr[$i],
          'LPSTELEVISI'=>$lpstelevisi[$i],
          'PERSENTASELPSTV' => $persentaselpstv[$i],
          'LPB' => $lpb[$i],
          'PERSENTASELPB' => $persentaselpb[$i],
        );

      }
      PemusatanKepemilikanSilang::insert($Pemusatan_KepemilikanSilangs);

    }

    $PemegangSaham = PemegangSaham::find($IDPERUSAHAANFINAL);

    if(isset($PemegangSaham) && $PemegangSaham->delete($IDPERUSAHAANFINAL)){
      $namapemegangsahams = $request->input('NAMA_PS');
      $banyaksaham = $request->input('BANYAK');
      $persentasesaham = $request->input('PRESENTASE');
      $Pemegang_Saham = array();

      for($i=0; $i<count($namapemegangsahams); $i++){
        // dd($request->input());
        $Pemegang_Saham[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMA_PEMEGANGSAHAM' => $namapemegangsahams[$i],
          'LEMBAR' => $banyaksaham[$i],
          'PERSENTASE' => $persentasesaham[$i],
        );

      }
      PemegangSaham::insert($Pemegang_Saham);

    }
    else if($PemegangSaham == null){
      $namapemegangsahams = $request->input('NAMA_PS');
      $banyaksaham = $request->input('BANYAK');
      $persentasesaham = $request->input('PRESENTASE');
      $Pemegang_Sahams = array();
      for($i=0; $i<count($namapemegangsahams); $i++){
        // dd($request->input());
        $Pemegang_Sahams[$i] = array(
          'IDPERUSAHAANFINAL'=>$IDPERUSAHAANFINAL,
          'NAMA_PEMEGANGSAHAM' => $namapemegangsahams[$i],
          'LEMBAR' => $banyaksaham[$i],
          'PERSENTASE' => $persentasesaham[$i],
        );

      }
      PemegangSaham::insert($Pemegang_Sahams);

    }

    return response()->json([
      'message' => 'Successfull update data'
    ]);
  }

}
