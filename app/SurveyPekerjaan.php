<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyPekerjaan extends Model {
    protected $table = 'surveypekerjaan';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'PNS',
      'PEGAWAISWASTA',
      'WIRASWASTA',
      'PENSIUNAN',
      'PELAJAR',
      'MAHASISWA',
      'IRT',
      'LAINNYA',
      'TIDAKBEKERJA',
      'JENISSURVEY',
      'tgl_perpanjangan',
      'tgl_perubahan',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
