<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Direktur extends Model {
    protected $table = 'direktur';
    protected $primaryKey = 'IDPERUSAHAANFINAL';
    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'NAMAKETUA',
      'TTLKETUA',
      'KEWARGANEGARAAN',
      'PENDIDIKAN',
      'NOTELP',
      'NOHP',
      'FAX',
      'EMAIL',
      'TMP_LAHIR',
      'tgl_perpanjangan',
      'tgl_perubahan'
  ];

    public $timestamps = false;

    // public static $rules = [
    //     'status' => 'required',
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NAMAKETUA' => 'required',
    //     'TTLKETUA' => 'required',
    //     'KEWARGANEGARAAN' => 'required',
    //     'PENDIDIKAN' => 'required',
    //     'NOTELP' => 'required',
    //     'NOHP' => 'required',
    //     'FAX' => 'required',
    //     'EMAIL' => 'required',
    //     'TMP_LAHIR' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
