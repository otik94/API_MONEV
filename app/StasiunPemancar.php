<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StasiunPemancar extends Model {
    protected $table = 'stasiunpemancar';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'NO',
      'NMSTASIUNPEMANCAR',
      'TGLOPERASI',
      'ALAMATSTASIUN',
      'PROPSTASIUN',
      'KANSTASIUN',
      'KECSTASIUN',
      'KELSTASIUN',
      'NOTELP',
      'EMAIL',
      'TINGGILOKASI',
      'KOORDINAT',
      'SISTEM_HUBUNGAN',
      'IDPERUSAHAANFINAL',
      'tgl_perpanjangan',
      'tgl_perubahan',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
