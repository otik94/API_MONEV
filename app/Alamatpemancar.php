<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Alamatpemancar extends Model {
    protected $table = 'alamatpemancar';
    protected $primaryKey = 'IDPERUSAHAANFINAL';
    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'ALAMATPEMANCAR',
      'PROPPEMANCAR',
      'KABPEMANCAR',
      'KECPEMANCAR',
      'KELPEMANCAR',
      'NOTELP',
      'TINGGILOKASI',
      'KOORDINAT',
      'KODEPOS',
      'FAX',
      'tgl_perubahan',
      'tgl_perpanjangan',
      'longitude',
      'latitude',
      'long_deg',
      'lat_deg',
      'long_minutes',
      'lat_minutes',
      'long_seconds',
      'lat_seconds',
      'long_direction',
      'lat_direction'
  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'ALAMATPEMANCAR' => 'required',
    //     'PROPPEMANCAR' => 'reuired',
    //     'KABPEMANCAR' => 'required',
    //     'KECPEMANCAR' => 'required',
    //     'KELPEMANCAR' => 'required',
    //     'NOTELP' => 'required',
    //     'TINGGILOKASI' => 'required',
    //     'KOORDINAT' => 'required',
    //     'KODEPOS' => 'required',
    //     'FAX' => 'required',
    //     'tgl_perubahan' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'longitude' => 'required',
    //     'latitude' => 'required',
    //     'long_deg' => 'required',
    //     'lat_deg' => 'required',
    //     'long_minutes' => 'required',
    //     'lat_minutes' => 'required',
    //     'long_seconds' => 'required',
    //     'lat_seconds' => 'required',
    //     'long_direction' => 'required',
    //     'lat_direction' => 'required'
    //
    // ];

}
