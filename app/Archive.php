<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model {
    protected $table = 'archive';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'IDPERUSAHAANFINAL',
      'data',
      'date_create',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
