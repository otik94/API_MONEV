<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Aktivitas extends Model {
    protected $table = 'aktivitas';

    protected $fillable = [
      'idaktivitas',
      'nmaktivitas',
      'keterangan',
      'username',
      'chgdate'


  ];

    public $timestamps = false;

    public static $rules = [
        'idaktivitas' => 'required',
        'nmaktivitas' => 'required',
        'keterangan' => 'required',
        'username' => 'required',
        'chgdate' => 'required',


    ];

}
