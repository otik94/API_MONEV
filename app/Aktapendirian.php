<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Aktapendirian extends Model {
    protected $table = 'aktapendirian';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'NOAKTA',
      'TANGGALPENDIRIAN',
      'NAMANOTARIS',
      'DOMISILINOTARIS',
      'tgl_perpanjangan',
      'tgl_perubahan'

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NOAKTA' => 'required',
    //     'TANGGALPENDIRIAN' => 'required',
    //     'NAMANOTARIS' => 'required',
    //     'DOMISILINOTARIS' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
