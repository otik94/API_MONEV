<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AlamatStasiunPengendali extends Model {
    protected $table = 'alamatstasiunpengendali';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'ALAMATSTASIUN',
      'PROPSTASIUN',
      'KABSTASIUN',
      'KECSTASIUN',
      'KELSTASIUN',
      'KODEPOS',
      'NOTELP',
      'STATUS',
      'FAX',
      'TINGGILOKASI',
      'KOORDINAT',
      'tgl_perpanjangan',
      'tgl_perubahan',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
