<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model {
    protected $table = 'contactperson';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'NAMA',
      'NOTELP',
      'NOFAX',
      'NOHP',
      'EMAIL'

    ];

    public $timestamps = false;

    public static $rules = [
        'NAMA' => 'required',
        'NOTELP' => 'required',
        'NOFAX' => 'required',
        'NOHP' => 'required',
        'EMAIL' => 'required'
    ];

    // public function setujui_perdat($id){
    //         $data = $this->db->join('users', 'users.IDPERUSAHAANFINAL = tbl_perubahan_otik.id_perusahaan')
    //                    ->get_where('tbl_perubahan_otik', array('users.id' =>$id, 'status_evaluasi' => '200'))
    //                    ->row();
    //         $data = unserialize($data->data);
    //         //create input post
    //         foreach ($data as $key => $value) {
    //           $_POST[$key] = $value;
    //         }
    //
    //         $IDPERUSAHAANFINAL 	= $this->input_post('IDPERUSAHAANFINAL');
    //         $des_email         	= $this->input_post('EMAIL_CP');
    //         $des_hp            	= $this->input_post('NOHP_CP');
    //
    //           $data_users = array(
    //             'email' => $this->input_post('EMAIL_CP'),
    //             );

              // $data_alamatkantor = array(
              //   'NMPERUSAHAAN' => $this->input_post('NMPERUSAHAAN'),
              //   'ALAMATKANTOR' => $this->input_post('ALAMATKANTOR'),
              //   'PROPKANTOR' => $this->input_post('PROPKANTOR'),
              //   'KABKANTOR' => $this->input_post('KABKANTOR'),
              //   'KECKANTOR' => $this->input_post('KECKANTOR'),
              //   'KELKANTOR' => $this->input_post('KELKANTOR'),
              //   'KODEPOS' => $this->input_post('KODEPOSKANTOR'),
              //   'NOTELPKANTOR' => $this->input_post('NOTELPKANTOR'),
              //   'NOHPKANTOR' => $this->input_post('NOHPKANTOR'),
              //   'NPWP' => $this->input_post('NPWP'),
              //   'WEBSITE' => $this->input_post('WEBSITE'),
              //   'ZONA' => $this->input_post('ZONA'),
              //   );

                //   if (!empty($_POST['NAMAKANAL_SUMBER'])) {
                //     $no_sumber =$_POST['NO_SUMBER'];
                //     $namakanal_sumber =$_POST['NAMAKANAL_SUMBER'];
                //     $dalamnegeri_sumber =$_POST['DALAMNEGERI_SUMBER'];
                //     $data_sumbermaterilpb = array();
                //     $data =array();
                //     for($i=0; $i<count($dalamnegeri_sumber); $i++) {
                //             $data_sumbermaterilpb[$i] = array(
                //                'NAMAPERUSAHAAN' => $namakanal_sumber[$i],
                //                'NAMAKANAL' => $dalamnegeri_sumber[$i],
                //                );
                //     }
                // }

                // if (!empty($_POST['NAMAKANAL_IKLAN'])) {
                //     $namakanal_iklan =$_POST['NAMAKANAL_IKLAN'];
                //     $persentase_iklan =$_POST['PERSENTASE_IKLAN'];
                //     $data_iklankomersial = array();
                //     $data =array();
                //     for($i=0; $i<count($persentase_iklan); $i++) {
                //             $data_iklankomersial[$i] = array(
                //                'NAMAKANAL' => $namakanal_iklan[$i],
                //                'PERSENTASE' => $persentase_iklan[$i],
                //                );
                //     }
                // }
                //
                // if (!empty($_POST['NAMAKANAL_UMUM'])) {
                //     $namakanal_umum =$_POST['NAMAKANAL_UMUM'];
                //     $dalamnegeri_umun =$_POST['DALAMNEGERI_UMUM'];
                //     $data_totalkanalumum = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_umum); $i++) {
                //             $data_totalkanalumum[$i] = array(
                //                'NAMAKANAL' => $namakanal_umum[$i],
                //                'ASALKANAL' => $dalamnegeri_umun[$i],
                //                );
                //     }
                // }

                // if (!empty($_POST['NAMAKANAL_BERITA'])) {
                //     $namakanal_berita =$_POST['NAMAKANAL_BERITA'];
                //     $dalamnegeri_berita =$_POST['DALAMNEGERI_BERITA'];
                //     $data_totalkanalberita = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_berita); $i++) {
                //             $data_totalkanalberita[$i] = array(
                //                'NAMAKANAL' => $namakanal_berita[$i],
                //                'ASALKANAL' => $dalamnegeri_berita[$i],
                //                );
                //     }
                // }

                // if (!empty($_POST['NAMAKANAL_MUSIK'])) {
                //     $namakanal_musik =$_POST['NAMAKANAL_MUSIK'];
                //     $dalamnegeri_musik =$_POST['DALAMNEGERI_MUSIK'];
                //     $data_totalkanalmusik = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_musik); $i++) {
                //             $data_totalkanalmusik[$i] = array(
                //                'NAMAKANAL' => $namakanal_musik[$i],
                //                'ASALKANAL' => $dalamnegeri_musik[$i],
                //                );
                //     }
                // }
                //
                // if (!empty($_POST['NAMAKANAL_OLAHRAGA'])) {
                //   $namakanal_olahraga =$_POST['NAMAKANAL_OLAHRAGA'];
                //     $dalamnegeri_olahraga =$_POST['DALAMNEGERI_OLAHRAGA'];
                //     $data_totalkanalolahraga = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_olahraga); $i++) {
                //             $data_totalkanalolahraga[$i] = array(
                //                'NAMAKANAL' => $namakanal_olahraga[$i],
                //                'ASALKANAL' => $dalamnegeri_olahraga[$i],
                //                );
                //     }
                // }
                //
                // if (!empty($_POST['NAMAKANAL_PENDIDIKAN'])) {
                //     $namakanal_pendidikan =$_POST['NAMAKANAL_PENDIDIKAN'];
                //     $dalamnegeri_pendidikan =$_POST['DALAMNEGERI_PENDIDIKAN'];
                //     $data_totalkanalpendidikan = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_pendidikan); $i++) {
                //             $data_totalkanalpendidikan[$i] = array(
                //                'NAMAKANAL' => $namakanal_pendidikan[$i],
                //                'ASALKANAL' => $dalamnegeri_pendidikan[$i],
                //                );
                //     }
                // }
                //
                // if (!empty($_POST['NAMAKANAL_DAKWAH'])) {
                //     $namakanal_dakwah =$_POST['NAMAKANAL_DAKWAH'];
                //     $dalamnegeri_dakwah =$_POST['DALAMNEGERI_DAKWAH'];
                //     $data_totalkanaldakwah = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_dakwah); $i++) {
                //             $data_totalkanaldakwah[$i] = array(
                //                'NAMAKANAL' => $namakanal_dakwah[$i],
                //                'ASALKANAL' => $dalamnegeri_dakwah[$i],
                //                );
                //     }
                // }
                //
                //
                // if (!empty($_POST['NAMAKANAL_FILM'])) {
                //     $namakanal_film =$_POST['NAMAKANAL_FILM'];
                //     $dalamnegeri_film =$_POST['DALAMNEGERI_FILM'];
                //     $data_totalkanalfilm = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_film); $i++) {
                //             $data_totalkanalfilm[$i] = array(
                //                'NAMAKANAL' => $namakanal_film[$i],
                //                'ASALKANAL' => $dalamnegeri_film[$i],
                //                );
                //     }
                // }
                //
                // if (!empty($_POST['NAMAKANAL_LAINNYA'])) {
                //     $namakanal_lainnya =$_POST['NAMAKANAL_LAINNYA'];
                //     $dalamnegeri_lainnya =$_POST['DALAMNEGERI_LAINNYA'];
                //     $data_totalkanallainnya = array();
                //     $data =array();
                //     for($i=0; $i<count($namakanal_lainnya); $i++) {
                //             $data_totalkanallainnya[$i] = array(
                //                'NAMAKANAL' => $namakanal_lainnya[$i],
                //                'ASALKANAL' => $dalamnegeri_lainnya[$i],
                //                );
                //     }
                // }
                //
                // if (!empty($_POST['NAMAPEMEGANGSAHAM'])) {
                //     $namapemegangsaham =$_POST['NAMAPEMEGANGSAHAM'];
                //     $mediacetak =$_POST['MEDIACETAK'];
                //     $persentasemc =$_POST['PERSENTASEMC'];
                //     $lpsradio =$_POST['LPSRADIO'];
                //     $persentaselpsr =$_POST['PERSENTASELPSR'];
                //     $lpstelevisi =$_POST['LPSTELEVISI'];
                //     $persentaselpstv =$_POST['PERSENTASELPSTV'];
                //     $lpb =$_POST['LPB'];
                //     $persentaselpb =$_POST['PERSENTASELPB'];
                //     $data_pemusatankepemilikansilang = array();
                //     $data =array();
                //     for($i=0; $i<count($namapemegangsaham); $i++) {
                //             $data_pemusatankepemilikansilang[$i] = array(
                //                'NAMAPEMEGANGSAHAM' => $namapemegangsaham[$i],
                //                'MEDIACETAK' => $mediacetak[$i],
                //                'PERSENTASEMC' => $persentasemc[$i],
                //                'LPSRADIO' => $lpsradio[$i],
                //                'PERSENTASELPSR' => $persentaselpsr[$i],
                //                'LPSTELEVISI' => $lpstelevisi[$i],
                //                'PERSENTASELPSTV' => $persentaselpstv[$i],
                //                'LPB' => $lpb[$i],
                //                'PERSENTASELPB' => $persentaselpb[$i],
                //                );
                //     }
                // }
                //
                //   if (!empty($_POST['NAMA_PS'])) {
                //       $namapemegangsahams =$_POST['NAMA_PS'];
                //       $banyaksaham =$_POST['BANYAK'];
                //       $persentasesaham =$_POST['PRESENTASE'];
                //       $data_pemegangsaham = array();
                //       for($i=0; $i<count($namapemegangsahams); $i++) {
                //               $data_pemegangsaham[$i] = array(
                //                  'NAMA_PEMEGANGSAHAM' => $namapemegangsahams[$i],
                //                  'LEMBAR' => $banyaksaham[$i],
                //                  'PERSENTASE' => $persentasesaham[$i],
                //                  );
                //       }
                //   }
                //
                //   $komposisi = array (
                //       'WNI' => $this->input_post('WNI'),
                //       'WNA' => $this->input_post('WNA'),
                //   );

              // $data_studio = array(
              //         'PROPSTUDIO' => $this->input_post('PROPSTUDIO'),
              //         'ALAMATSTUDIO' => $this->input_post('ALAMATSTUDIO'),
              //         'KABSTUDIO' => $this->input_post('KABSTUDIO'),
              //         'KECSTUDIO' => $this->input_post('KECSTUDIO'),
              //         'KELSTUDIO' => $this->input_post('KELSTUDIO'),
              //         'KODEPOS' => $this->input_post('KODEPOSSTUDIO'),
              //         'NOTELP' => $this->input_post('NOTELPSTUDIO'),
              //       );

                  // $data_pemancar = array(
                  //     'ALAMATPEMANCAR' => $this->input_post('ALAMATPEMANCAR'),
                  //     'PROPPEMANCAR' => $this->input_post('PROPPEMANCAR'),
                  //     'KABPEMANCAR' => $this->input_post('KABPEMANCAR'),
                  //     'KECPEMANCAR' => $this->input_post('KECPEMANCAR'),
                  //     'KELPEMANCAR' => $this->input_post('KELPEMANCAR'),
                  //     'KODEPOS' => $this->input_post('KODEPOSPEMANCAR'),
                  //     'FAX' => $this->input_post('FAX_AP'),
                  //     'TINGGILOKASI' => $this->input_post('TINGGILOKASI_AP'),
                  //     'KOORDINAT' => $this->input_post('KOORDINAT_AP'),
                  //     'longitude' => $this->input_post('longitude'),
                  //     'latitude' => $this->input_post('latitude'),
                  //     'NOTELP' => $this->input_post('NOTELP_AP'),
                  //     'long_deg' => $this->input_post('long_deg'),
                  //     'lat_deg' => $this->input_post('lat_deg'),
                  //     'long_minutes' => $this->input_post('long_minutes'),
                  //     'lat_minutes' => $this->input_post('lat_minutes'),
                  //     'long_seconds' => $this->input_post('long_seconds'),
                  //     'lat_seconds' => $this->input_post('lat_seconds'),
                  //     'long_direction' => $this->input_post('long_direction'),
                  //     'lat_direction' => $this->input_post('lat_direction')
                  //   );

                  // $data_contact_person = array(
                  //     'NAMA' => $this->input_post('NAMA_CP'),
                  //     'NOTELP' => $this->input_post('NOTELP_CP'),
                  //     'NOFAX' => $this->input_post('NOFAX_CP'),
                  //     'NOHP' => $this->input_post('NOHP_CP'),
                  //     'EMAIL' => $this->input_post('EMAIL_CP'),
                  //   );

                //   $data_aktapendirian = array(
                //     'NOAKTA' => $this->input_post('NOAKTA_AP'),
                //     'TANGGALPENDIRIAN' => $this->input_post('TANGGALPENDIRIAN_AP'),
                //     'NAMANOTARIS' => $this->input_post('NAMANOTARIS_AP'),
                //     'DOMISILINOTARIS' => $this->input_post('DOMISILINOTARIS_AP'),
                //     );
                //
                //   $data_aktaperubahan = array(
                //     'NOPERUBAHAN' => $this->input_post('NOPERUBAHAN_APT'),
                //     'TANGGALPERUBAHAN' => $this->input_post('TANGGALPERUBAHAN_APT'),
                //     'NAMAINSTANSI' => $this->input_post('NAMAINSTANSI_APT'),
                //     'DOMISILIINSTANSI' => $this->input_post('DOMISILIINSTANSI_APT'),
                //     );
                //
                //
                //   $data_aspekmodallpb = array(
                //     'MODALDASAR' => $this->input_post('MODALDASAR'),
                //     'BANYAKSAHAM' => $this->input_post('JMLSAHAM'),
                //     'NOMINALSAHAM' => $this->input_post('NOMINALSAHAM'),
                //     'KOMPOSISI' => $this->input_post('KOMPOSISI'),
                //     // 'MODALDITEMPATKAN' => $this->input_post('MODALDITEMPATKAN'),
                //     'MODALSETOR' => $this->input_post('MODALSETOR'),
                //     );
                //
                //   $data_aspekpermodalan = array(
                //     'MODALDASAR' => $this->input_post('MODALDASAR'),
                //     'JMLSAHAM' => $this->input_post('JMLSAHAM'),
                //     'NOMINALSAHAM' => $this->input_post('NOMINALSAHAM'),
                //     'KOMPOSISI' => $this->input_post('KOMPOSISI'),
                //     'MODALSETOR' => $this->input_post('MODALSETOR'),
                //     'MODALDITEMPATKAN' => $this->input_post('MODALDITEMPATKAN'),
                //     );
                //
                //   $data_aspekpermodalankom = array(
                //     'SIMPANANPOKOK' => $this->input_post('SIMPANANPOKOK'),
                //     // 'SIMPANANWAJIB' => $this->input_post('SIMPANANWAJIB'),
                //     'SIMPANANSUKARELA' => $this->input_post('SIMPANANSUKARELA'),
                //     'JMLANGGOTA' => $this->input_post('JMLANGGOTA'),
                //     'JMLMODALAWAL' => $this->input_post('JMLMODALAWAL'),
                //     );
                //
                //   $data_aspekpermodalankomsumber = array(
                //     'SUMBANGAN' => $this->input_post('SUMBANGAN'),
                //     'HIBAH' => $this->input_post('HIBAH'),
                //     'SPONSOR' => $this->input_post('SPONSOR'),
                //     'LAINNYA' => $this->input_post('LAINNYA_AP'),
                //     'SEBUTKAN' => $this->input_post('SEBUTKAN_AP'),
                //     );
                //
                //   $data_stasiun = array(
                //       'PROPSTASIUN' => $this->input_post('PROPSTASIUN'),
                //       'ALAMATSTASIUN' => $this->input_post('ALAMATSTASIUN'),
                //       'KABSTASIUN' => $this->input_post('KABSTASIUN'),
                //       'KECSTASIUN' => $this->input_post('KECSTASIUN'),
                //       'KELSTASIUN' => $this->input_post('KELSTASIUN'),
                //       'KODEPOS' => $this->input_post('KODEPOSSTASIUN'),
                //       'NOTELP' => $this->input_post('NOTELPSTASIUN'),
                //       'FAX' => $this->input_post('FAXSTASIUN'),
                //       'TINGGILOKASI' => $this->input_post('TINGGILOKASI_AS'),
                //       'KOORDINAT' => $this->input_post('KOORDINAT_AS'),
                //   );
                //
                //   $data_dppemberitaan = array(
                //   'PSTETAP' => $this->input_post('11'),
                //   'PSTIDAK' => $this->input_post('12'),
                //   'SARJANATETAP' => $this->input_post('13'),
                //   'SARJANATIDAK' => $this->input_post('14'),
                //   'DIPLOMATETAP' => $this->input_post('15'),
                //   'DIPLOMATIDAK' => $this->input_post('16'),
                //   'SLTATETAP' => $this->input_post('17'),
                //   'SLTATIDAK' => $this->input_post('18'),
                //   'SLTPTETAP' => $this->input_post('19'),
                //   'SLTPTIDAK' => $this->input_post('20'),
                //   'SDTETAP' => $this->input_post('53'),
                //   'SDTIDAK' => $this->input_post('54'),
                //   'TOTALTETAP' => $this->input_post('BERITATOTAL_TETAP'),
                //   'TOTALTIDAK' => $this->input_post('BERITATOTAL_TIDAK'),
                //   );
                //
                // $data_dpsiaran = array(
                //   'PSTETAP' => $this->input_post('1'),
                //   'PSTIDAK' => $this->input_post('2'),
                //   'SARJANATETAP' => $this->input_post('3'),
                //   'SARJANATIDAK' => $this->input_post('4'),
                //   'DIPLOMATETAP' => $this->input_post('5'),
                //   'DIPLOMATIDAK' => $this->input_post('6'),
                //   'SLTATETAP' => $this->input_post('7'),
                //   'SLTATIDAK' => $this->input_post('8'),
                //   'SLTPTETAP' => $this->input_post('9'),
                //   'SLTPTIDAK' => $this->input_post('10'),
                //   'SDTETAP' => $this->input_post('51'),
                //   'SDTIDAK' => $this->input_post('52'),
                //   'TOTALTETAP' => $this->input_post('PROGSITOTAL_TETAP'),
                //   'TOTALTIDAK' => $this->input_post('PROGSITOTAL_TIDAK'),
                //   );
                //
                // $data_dptatausaha = array(
                //   'PSTETAP' => $this->input_post('41'),
                //   'PSTIDAK' => $this->input_post('42'),
                //   'SARJANATETAP' => $this->input_post('43'),
                //   'SARJANATIDAK' => $this->input_post('44'),
                //   'DIPLOMATETAP' => $this->input_post('45'),
                //   'DIPLOMATIDAK' => $this->input_post('46'),
                //   'SLTATETAP' => $this->input_post('47'),
                //   'SLTATIDAK' => $this->input_post('48'),
                //   'SLTPTETAP' => $this->input_post('49'),
                //   'SLTPTIDAK' => $this->input_post('50'),
                //   'SDTETAP' => $this->input_post('59'),
                //   'SDTIDAK' => $this->input_post('60'),
                //   'TOTALTETAP' => $this->input_post('TUTOTAL_TETAP'),
                //   'TOTALTIDAK' => $this->input_post('TUTOTAL_TIDAK'),
                //   );
                //
                // $data_dpteknikstudio = array(
                //   'PSTETAP' => $this->input_post('21'),
                //   'PSTIDAK' => $this->input_post('22'),
                //   'SARJANATETAP' => $this->input_post('23'),
                //   'SARJANATIDAK' => $this->input_post('24'),
                //   'DIPLOMATETAP' => $this->input_post('25'),
                //   'DIPLOMATIDAK' => $this->input_post('26'),
                //   'SLTATETAP' => $this->input_post('27'),
                //   'SLTATIDAK' => $this->input_post('28'),
                //   'SLTPTETAP' => $this->input_post('29'),
                //   'SLTPTIDAK' => $this->input_post('30'),
                //   'SDTETAP' => $this->input_post('55'),
                //   'SDTIDAK' => $this->input_post('56'),
                //   'TOTALTETAP' => $this->input_post('TEKSUTOTAL_TETAP'),
                //   'TOTALTIDAK' => $this->input_post('TEKSUTOTAL_TIDAK'),
                //   );
                //
                // $data_dptekniktransmisi = array(
                //   'PSTETAP' => $this->input_post('31'),
                //   'PSTIDAK' => $this->input_post('32'),
                //   'SARJANATETAP' => $this->input_post('33'),
                //   'SARJANATIDAK' => $this->input_post('34'),
                //   'DIPLOMATETAP' => $this->input_post('35'),
                //   'DIPLOMATIDAK' => $this->input_post('36'),
                //   'SLTATETAP' => $this->input_post('37'),
                //   'SLTATIDAK' => $this->input_post('38'),
                //   'SLTPTETAP' => $this->input_post('39'),
                //   'SLTPTIDAK' => $this->input_post('40'),
                //   'SDTETAP' => $this->input_post('57'),
                //   'SDTIDAK' => $this->input_post('58'),
                //   'TOTALTETAP' => $this->input_post('TEKTRANSTOTAL_TETAP'),
                //   'TOTALTIDAK' => $this->input_post('TEKTRANSTOTAL_TIDAK'),
                //   );
                //
                // $data_dptotalpengurus = array(
                //   //'IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL
                //   'PSTETAP' => $this->input_post('PSTOTAL_TETAP'),
                //   'PSTIDAK' => $this->input_post('PSTOTAL_TDKTETAP'),
                //   'SARJANATETAP' => $this->input_post('SARJANATOTAL_TETAP'),
                //   'SARJANATIDAK' => $this->input_post('SARJANATOTAL_TIDAK'),
                //   'DIPLOMATETAP' => $this->input_post('DIPLOMATOTAL_TETAP'),
                //   'DIPLOMATIDAK' => $this->input_post('DIPLOMATOTAL_TIDAK'),
                //   'SLTATETAP' => $this->input_post('SLTATOTAL_TETAP'),
                //   'SLTATIDAK' => $this->input_post('SLTATOTAL_TIDAK'),
                //   'SLTPTETAP' => $this->input_post('SLTPTOTAL_TETAP'),
                //   'SLTPTIDAK' => $this->input_post('SLTPTOTAL_TIDAK'),
                //   'SDTETAP' => $this->input_post('SDTOTAL_TETAP'),
                //   'SDTIDAK' => $this->input_post('SDTOTAL_TIDAK'),
                //   'TOTALTETAP' => $this->input_post('TOTAL_TETAP'),
                //   'TOTALTIDAK' => $this->input_post('TOTAL_TIDAK'),
                //   );
                //
                //   $data_dpk = array(
                //     'NAMAKETUA' => $this->input_post('DPK_NAMA'),
                //     'TTLKETUA' => $this->input_post('DPK_TTL'),
                //     'TMP_LAHIR' => $this->input_post('DPK_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('DPK_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('DPK_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('DPK_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('DPK_NOHP'),
                //     'FAX' => $this->input_post('DPK_FAX'),
                //     'EMAIL' => $this->input_post('DPK_EMAIL'),
                //     );
                //
                //   $data_adpk = array(
                //     'NAMAKETUA' => $this->input_post('ADPK_NAMA'),
                //     'TTLKETUA' => $this->input_post('ADPK_TTL'),
                //     'TMP_LAHIR' => $this->input_post('ADPK_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('ADPK_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('ADPK_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('ADPK_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('ADPK_NOHP'),
                //     'FAX' => $this->input_post('ADPK_FAX'),
                //     'EMAIL' => $this->input_post('ADPK_EMAIL'),
                //     );
                //
                //   $data_dpengawas = array(
                //     'NAMAKETUA' => $this->input_post('KDP_NAMA'),
                //     'TTLKETUA' => $this->input_post('KDP_TTL'),
                //     'TMP_LAHIR' => $this->input_post('KDP_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('KDP_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('KDP_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('KDP_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('KDP_NOHP'),
                //     'FAX' => $this->input_post('KDP_FAX'),
                //     'EMAIL' => $this->input_post('KDP_EMAIL'),
                //     );
                //
                //   $data_adpengawas = array(
                //     'NAMAKETUA' => $this->input_post('ADP1_NAMA'),
                //     'TTLKETUA' => $this->input_post('ADP1_TTL'),
                //     'TMP_LAHIR' => $this->input_post('ADP1_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('ADP1_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('ADP1_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('ADP1_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('ADP1_NOHP'),
                //     'FAX' => $this->input_post('ADP1_FAX'),
                //     'EMAIL' => $this->input_post('ADP1_EMAIL'),
                //     );
                //
                //   $data_adpengawas2 = array(
                //     'NAMAKETUA' => $this->input_post('ADP2_NAMA'),
                //     'TTLKETUA' => $this->input_post('ADP2_TTL'),
                //     'TMP_LAHIR' => $this->input_post('ADP2_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('ADP2_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('ADP2_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('ADP2_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('ADP2_NOHP'),
                //     'FAX' => $this->input_post('ADP2_FAX'),
                //     'EMAIL' => $this->input_post('ADP2_EMAIL'),
                //     );
                //
                //   $data_ketuapendiri = array(
                //     'NAMAKETUA' => $this->input_post('KP_NAMA'),
                //     'TTLKETUA' => $this->input_post('KP_TTL'),
                //     'TMP_LAHIR' => $this->input_post('KP_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('KP_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('KP_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('KP_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('KP_NOHP'),
                //     'FAX' => $this->input_post('KP_FAX'),
                //     'EMAIL' => $this->input_post('KP_EMAIL'),
                //     );
                //
                //   $data_pendiri = array(
                //     'NAMAKETUA' => $this->input_post('P_NAMA'),
                //     'TTLKETUA' => $this->input_post('P_TTL'),
                //     'TMP_LAHIR' => $this->input_post('P_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('P_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('P_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('P_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('P_NOHP'),
                //     'FAX' => $this->input_post('P_FAX'),
                //     'EMAIL' => $this->input_post('P_EMAIL'),
                //     );
                //
                //   $data_direktur = array(
                //     'NAMAKETUA' => $this->input_post('DU_NAMA'),
                //     'TTLKETUA' => $this->input_post('DU_TTL'),
                //     'TMP_LAHIR' => $this->input_post('DU_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('DU_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('DU_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('DU_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('DU_NOHP'),
                //     'FAX' => $this->input_post('DU_FAX'),
                //     'EMAIL' => $this->input_post('DU_EMAIL'),
                //     );
                //
                //   $data_direktur_2 = array(
                //     'NAMAKETUA' => $this->input_post('D_NAMA'),
                //     'TTLKETUA' => $this->input_post('D_TTL'),
                //     'TMP_LAHIR' => $this->input_post('D_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('D_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('D_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('D_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('D_NOHP'),
                //     'FAX' => $this->input_post('D_FAX'),
                //     'EMAIL' => $this->input_post('D_EMAIL'),
                //     );
                //
                //   $data_komisarisutama = array(
                //     'NAMAKOMISARIS' => $this->input_post('KU_NAMA'),
                //     'TTLKOMISARIS' => $this->input_post('KU_TTL'),
                //     'TMP_LAHIR' => $this->input_post('KU_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('KU_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('KU_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('KU_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('KU_NOHP'),
                //     'FAX' => $this->input_post('KU_FAX'),
                //     'EMAIL' => $this->input_post('KU_EMAIL'),
                //     );
                //
                //   $data_komisaris = array(
                //     'NAMAKOMISARIS' => $this->input_post('KOMISARIS_NAMA'),
                //     'TTLKOMISARIS' => $this->input_post('KOMISARIS_TTL'),
                //     'TMP_LAHIR' => $this->input_post('KOMISARIS_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('KOMISARIS_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('KOMISARIS_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('KOMISARIS_TELP_KANTOR'),
                //     'NOHP' => $this->input_post('KOMISARIS_NOHP'),
                //     'FAX' => $this->input_post('KOMISARIS_FAX'),
                //     'EMAIL' => $this->input_post('KOMISARIS_EMAIL'),
                //     );
                //
                //   $data_komisaris2 = array(
                //     'NAMAKOMISARIS' => $this->input_post('KOMISARIS_NAMA2'),
                //     'TTLKOMISARIS' => $this->input_post('KOMISARIS_TTL2'),
                //     'TMP_LAHIR' => $this->input_post('KOMISARIS_TMP_LAHIR2'),
                //     'KEWARGANEGARAAN' => $this->input_post('KOMISARIS_KEWARGANEGARAAN2'),
                //     'PENDIDIKAN' => $this->input_post('KOMISARIS_PENDIDIKAN2'),
                //     'NOTELP' => $this->input_post('KOMISARIS_TELP_KANTOR2'),
                //     'NOHP' => $this->input_post('KOMISARIS_NOHP2'),
                //     'FAX' => $this->input_post('KOMISARIS_FAX2'),
                //     'EMAIL' => $this->input_post('KOMISARIS_EMAIL2'),
                //     );
                //
                //   $data_pjbidkeu = array(
                //     'NAMAPENANGUNGJAWAB' => $this->input_post('PJBK_NAMA'),
                //     'TTLPENANGGUNGJAWAB' => $this->input_post('PJBK_TTL'),
                //     'TMP_LAHIR' => $this->input_post('PJBK_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('PJBK_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('PJBK_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('PJBK_TELPKANTOR'),
                //     'NOHP' => $this->input_post('PJBK_NOHP'),
                //     'FAX' => $this->input_post('PJBK_FAX'),
                //     'EMAIL' => $this->input_post('PJBK_EMAIL'),
                //     );
                //
                //   $data_pjbidpemberitaan = array(
                //     'NAMAPENANGUNGJAWAB' => $this->input_post('PJBP_NAMA'),
                //     'TTLPENANGGUNGJAWAB' => $this->input_post('PJBP_TTL'),
                //     'TMP_LAHIR' => $this->input_post('PJBP_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('PJBP_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('PJBP_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('PJBP_TELPKANTOR'),
                //     'NOHP' => $this->input_post('PJBP_NOHP'),
                //     'FAX' => $this->input_post('PJBP_FAX'),
                //     'EMAIL' => $this->input_post('PJBP_EMAIL'),
                //     );
                //
                //   $data_pjbidsiaran = array(
                //     'NAMAPENANGUNGJAWAB' => $this->input_post('PJBS_NAMA'),
                //     'TTLPENANGGUNGJAWAB' => $this->input_post('PJBS_TTL'),
                //     'TMP_LAHIR' => $this->input_post('PJBS_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('PJBS_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('PJBS_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('PJBS_TELPKANTOR'),
                //     'NOHP' => $this->input_post('PJBS_NOHP'),
                //     'FAX' => $this->input_post('PJBS_FAX'),
                //     'EMAIL' => $this->input_post('PJBS_EMAIL'),
                //     );
                //
                //   $data_pjbidteknik = array(
                //     'NAMAPENANGUNGJAWAB' => $this->input_post('PJBT_NAMA'),
                //     'TTLPENANGGUNGJAWAB' => $this->input_post('PJBT_TTL'),
                //     'TMP_LAHIR' => $this->input_post('PJBT_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('PJBT_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('PJBT_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('PJBT_TELPKANTOR'),
                //     'NOHP' => $this->input_post('PJBT_NOHP'),
                //     'FAX' => $this->input_post('PJBT_FAX'),
                //     'EMAIL' => $this->input_post('PJBT_EMAIL'),
                //     );
                //
                //   $data_pjbidusaha = array(
                //     'NAMAPENANGUNGJAWAB' => $this->input_post('PJBU_NAMA'),
                //     'TTLPENANGGUNGJAWAB' => $this->input_post('PJBU_TTL'),
                //     'TMP_LAHIR' => $this->input_post('PJBU_TMP_LAHIR'),
                //     'KEWARGANEGARAAN' => $this->input_post('PJBU_KEWARGANEGARAAN'),
                //     'PENDIDIKAN' => $this->input_post('PJBU_PENDIDIKAN'),
                //     'NOTELP' => $this->input_post('PJBU_TELPKANTOR'),
                //     'NOHP' => $this->input_post('PJBU_NOHP'),
                //     'FAX' => $this->input_post('PJBU_FAX'),
                //     'EMAIL' => $this->input_post('PJBU_EMAIL'),
                //     );
                //
                //   $data_pendirian_lpplokal = array(
                //     'NO_LPP' => $this->input_post('PLPP_LOKAL_NO'),
                //     'TGL' => $this->input_post('PLPP_LOKAL_TGL'),
                //     'NMPENANGGUNGJAWAB' => $this->input_post('PLPP_LOKAL_NM_PJ'),
                //     );
                //
                //   $data_peng_akta_pendirian = array(
                //     'NOAKTA' => $this->input_post('NOAKTA_PAP'),
                //     'TGL' => $this->input_post('TANGGALPENGESAHAN_PAP'),
                //     'NMINSTANSI' => $this->input_post('NAMAINSTANSI_PAP'),
                //     );
                //
                //   $data_peng_akta_perubahan = array(
                //     'NOAKTA' => $this->input_post('NOAKTA_PAPT'),
                //     'TGL' => $this->input_post('TANGGALPENGESAHAN_PAPT'),
                //     'NMINSTANSI' => $this->input_post('NAMAINSTANSI_PAPT'),
                //     );
                //
                //   $data_surat_ket_domisili = array(
                //     'NOAKTA' => $this->input_post('NOSURAT_LPP'),
                //     'TGL' => $this->input_post('TANGGALSURAT_LPP'),
                //     'NMINSTANSI' => $this->input_post('NAMAINSTANSI_LPP'),
                //     );
                //
                //   $data_sistem_modulasi = array(
                //     'SISTEM_MODULASI' => $this->input_post('SISMOD'),
                //     // 'WILAYAH_SISMOD' => $this->input_post('WILAYAHLAYANAN_SISMOD'),
                //     );
                //
                //   if (is_kominfo() || is_admin()) {
                //     $data_sistem_modulasi['FREK'] = $this->input_post('FREK');
                //     $data_sistem_modulasi['KANAL'] = $this->input_post('KANAL');
                //   }
                //
                //   $data_stasiun_distribusi = array(
                //     'MULAI_BEROPERASI' => $this->input_post('MULAI_BEROPERASI'),
                //     'MODA_SUARA' => $this->input_post('MPS'),
                //     'SISTEM_HUBUNGAN' =>$this->input_post('SISHUB'),
                //     // 'WILAYAHJANGKUAN' =>$this->input_post('WILAYAHJANGKUAN_STD'),
                //     // 'WILAYAHLAYANAN' =>$this->input_post('WILAYAHLAYANAN_SDP'),
                //     'SDP' =>$this->input_post('SDP'),
                //     );
                //
                //   $data_stasiun_pemancar = array(
                //     'NMSTASIUNPEMANCAR' => $this->input_post('NAMA_SP'),
                //     'TGLOPERASI' => $this->input_post('MULAI_BEROPERASI'),
                //     'SISTEM_HUBUNGAN' =>$this->input_post('SISHUB'),
                //     );
                //
                //   $data_speksatelit = array(
                //     'NAMA' => $this->input_post('NAMA_SS'),
                //     'NEGARA' => $this->input_post('NEGARA_SS'),
                //     'RANGE' =>$this->input_post('RANGE_SS'),
                //     'LEBARPITA' =>$this->input_post('LEBARPITA_SS'),
                //     'KOORDINAT' =>$this->input_post('KOORDINAT_SS'),
                //     );


                  // $data_satelitpenyiaran = array(
                  //   'NAMA' => $this->input_post('NAMA_SATPEN'),
                  //   'NEGARA' => $this->input_post('NEGARA_SATPEN'),
                  //   'FREKDOWN' =>$this->input_post('FREAKDOWN_SATPEN'),
                  //   'LEBARPITA' =>$this->input_post('LEBARPITA_SATPEN'),
                  //   'KOORDINAT' =>$this->input_post('KOORDINAT_SATPEN'),
                  //   // 'WILAYAHJANGKAUAN' =>$this->input_post('WILAYAHJANGKAUAN_SATPEN'),
                  //   );

                //   $data_spekradio = array(
                //     'MEREK' => $this->input_post('MEREK_SPR'),
                //     'TIPE' => $this->input_post('TIPE_SPR'),
                //     'NOSERI' =>$this->input_post('NOSERI_SPR'),
                //     'BUATAN' =>$this->input_post('JENIS_SPR'),
                //     'TAHUN' =>$this->input_post('tahun_SPR'),
                //     'FREK' =>$this->input_post('FREK_SPR'),
                //     'LEBARFREK' =>$this->input_post('LEBARFREK_SPR'),
                //     );
                //
                //   $data_transponder = array(
                //     'JMLTRANSPONDER' => $this->input_post('JMLTRANSPONDER'),
                //     'BWTRANSPONDERA' => $this->input_post('BWTRANSPONDERA'),
                //     'BWTRANSPONDERZ' =>$this->input_post('BWTRANSPONDERZ'),
                //     'JMLSALURAN' =>$this->input_post('JMLSALURAN'),
                //     'FREKDOWNA' =>$this->input_post('FREKDOWNA'),
                //     'FREKDOWNZ' =>$this->input_post('FREKDOWNZ'),
                //     'FREKUPA' =>$this->input_post('FREKUPA'),
                //     'FREKUPZ' =>$this->input_post('FREKUPZ'),
                //     );
                //
                //   $data_perlpemancar = array(
                //     'MEREK' => $this->input_post('MEREK_PP'),
                //     'TIPE' => $this->input_post('TIPE_PP'),
                //     'NOSERI' =>$this->input_post('NOSERI_PP'),
                //     'BUATAN' =>$this->input_post('BUATAN_PP'),
                //     'TAHUN' =>$this->input_post('tahun_PP'),
                //     'DAYAPEMANCARMAKS' =>$this->input_post('DAYAOEMANCARMAX_PP'),
                //     'DAYAPEMANCARTERPASANG' =>$this->input_post('DAYAPEMANCARTERPASANG_PP'),
                //     );
                //
                //   $data_feeder = array(
                //     'JENIS' => $this->input_post('JENIS_FEEDER'),
                //     'MEREK' => $this->input_post('MEREK_FEEDER'),
                //     'TIPEUKURAN' => $this->input_post('TIPEUKURAN_UKURAN'),
                //     'PANJANGKABEL' => $this->input_post('PANJANGKABEL_FEEDER'),
                //     'LOSSKABEL' => $this->input_post('LOSSKABEL_FEEDER'),
                //     'TOTALLOSFEEDER' => $this->input_post('TOTALLOSSKABEL_FEEDER'),
                //     );
                //
                //   $data_formatsiaran = array(
                //     'FORMATSIARAN' => $this->input_post('FORMATSIARAN'),
                //     'LAINNYA' => $this->input_post('LAINNYA'),
                //     );
                //
                //   $data_surveyjk = array(
                //     'JENISSURVEY' => $this->input_post('JENISSURVEY_JK'),
                //     'PRIA' => $this->input_post('PRIA_JK'),
                //     'WANITA' => $this->input_post('WANITA_JK'),
                //     );
                //
                //   $data_surveyusia = array(
                //     'JENISSURVEY' => $this->input_post('JENISSURVEY_KU'),
                //     'BAWAH15' => $this->input_post('BAWAH15'),
                //     '16SD19' => $this->input_post('15SD19'),
                //     '20SD24' => $this->input_post('20SD24'),
                //     '25SD29' => $this->input_post('25SD29'),
                //     '30SD34' => $this->input_post('30SD34'),
                //     '35SD39' => $this->input_post('35SD39'),
                //     '40SD50' => $this->input_post('40SD50'),
                //     'ATAS50' => $this->input_post('ATAS50'),
                //     );
                //
                //   $data_surveypekerjaan = array(
                //     'JENISSURVEY' => $this->input_post('JENISSURVEY_PK'),
                //     'PNS' => $this->input_post('PNS'),
                //     'PEGAWAISWASTA' => $this->input_post('PEGAWAISWASTA'),
                //     'WIRASWASTA' => $this->input_post('WIRASWASTA'),
                //     'PENSIUNAN' => $this->input_post('PENSIUNAN'),
                //     'PELAJAR' => $this->input_post('PELAJAR'),
                //     'MAHASISWA' => $this->input_post('MAHASISWA'),
                //     'IRT' => $this->input_post('IRT'),
                //     'LAINNYA' => $this->input_post('LAINNYA'),
                //     'TIDAKBEKERJA' => $this->input_post('TIDAKBEKERJA'),
                //     );
                //
                //   $data_surveypendidikan = array(
                //     'JENISSURVEY' => $this->input_post('JENISSURVEY_PT'),
                //     'TIDAKSD' => $this->input_post('TIDAKSD'),
                //     'SD' => $this->input_post('SD'),
                //     'SLTP' => $this->input_post('SLTP'),
                //     'SLTA' => $this->input_post('SLTA'),
                //     'AKADEMI' => $this->input_post('AKADEMI'),
                //     'PT' => $this->input_post('PT'),
                //     );
                //
                //   $data_surveyekonomi = array(
                //     'JENISSURVEY' => $this->input_post('JENISSURVEY_E'),
                //     'ATAS3JT' => $this->input_post('ATAS3JT'),
                //     '2JTSD3JT' => $this->input_post('2JTSD3JT'),
                //     '1JTSD2JT' => $this->input_post('1JTSD2JT'),
                //     '7SD1JT' => $this->input_post('7SD1JT'),
                //     '5SD7' => $this->input_post('5SD7'),
                //     'BAWAH5' => $this->input_post('BAWAH5'),
                //     );
                //
                //   // if(is_array($this->input_post('WILAYAHLAYANAN_TV'))) {
                //   //         $data_wilayahlayanan['WILAYAHLAYANAN']  = "";
                //   //         foreach($this->input_post('WILAYAHLAYANAN_TV') as $row){
                //   //             $data_wilayahlayanan['WILAYAHLAYANAN'] .= $row;
                //   //             if ($row != end($_POST['WILAYAHLAYANAN_TV'])) {
                //   //                 $data_wilayahlayanan['WILAYAHLAYANAN'] .= ";";
                //   //             }
                //   //         }
                //   //     }else{
                //   //         $data_wilayahlayanan['WILAYAHLAYANAN'] = $this->input_post('WILAYAHLAYANAN_RADIO');
                //   //     }
                //
                //   $data_sumbermateri = array(
                //     'SENDIRI' => $this->input_post('SENDIRI_SMAS'),
                //     'AKUISISI' => $this->input_post('AKUISISI_SMAS'),
                //     'KERJASAMA' => $this->input_post('KERJASAMA_SMAS'),
                //     );
                //
                //   $data_waktusiaranlpb = array(
                //     'KANAL24JAM' => $this->input_post('KANAL24JAM'),
                //     'KANALKURANG24JAM' => $this->input_post('KANALKURANG24JAM'),
                //     );
                //
                //   $data_persentasesiaranlpb = array(
                //     'UMUM' => $this->input_post('ms_lpb_umum'),
                //     'BERITA' => $this->input_post('ms_lpb_berita'),
                //     'PENDIDIKAN' => $this->input_post('ms_lpb_pendidikan'),
                //     'AGAMA' => $this->input_post('ms_lpb_agama'),
                //     'OLAHRAGA' => $this->input_post('ms_lpb_olahraga'),
                //     'FILM' => $this->input_post('ms_lpb_film'),
                //     'MUSIK' => $this->input_post('ms_lpb_musik'),
                //     'LAINNYA' => $this->input_post('ms_lpb_lainnya'),
                //     );
                //
                //   $data_persentasesiaran = array(
                //     'BERITA' => $this->input_post('BERITA'),
                //     'INFORMASI' => $this->input_post('INFORMASI'),
                //     'PENDIDIKAN' => $this->input_post('PENDIDIKAN'),
                //     'AGAMA' => $this->input_post('AGAMA'),
                //     'OLAHRAGA' => $this->input_post('OLAHRAGA'),
                //     'HIBURAN' => $this->input_post('HIBURAN'),
                //     'IKLAN' => $this->input_post('IKLAN'),
                //     'LAYANANMASY' => $this->input_post('LAYANANMASY'),
                //     );
                //
                //   $data_persentasesiaranmusik = array(
                //     'INDONESIA' => $this->input_post('INDONESIA'),
                //     'DANGDUT' => $this->input_post('DANGDUT'),
                //     'BARAT' => $this->input_post('BARAT'),
                //     'DAERAH' => $this->input_post('DAERAH'),
                //     'KERONCONG' => $this->input_post('KERONCONG'),
                //     'LAINNYA' => $this->input_post('LAINNYA_MUSIK'),
                //     );
                //
                //   $data_persentasesiaranmusik = array(
                //     'INDONESIA' => $this->input_post('INDONESIA'),
                //     'DANGDUT' => $this->input_post('DANGDUT'),
                //     'BARAT' => $this->input_post('BARAT'),
                //     'DAERAH' => $this->input_post('DAERAH'),
                //     'KERONCONG' => $this->input_post('KERONCONG'),
                //     'LAINNYA' => $this->input_post('LAINNYA_MUSIK'),
                //     );
                //
                //   $data_lpbpersentasesiaran = array(
                //     'LOKAL' => $this->input_post('LOKAL_PS'),
                //     'LINGKUNGANSENDIRI' => $this->input_post('LINGKUNGANSENDIRI_PS'),
                //     'LUARPERUSAHAAN' => $this->input_post('LUARPERUSAHAAN_PS'),
                //     'FREETOAIR' => $this->input_post('FREETOAIR_PS'),
                //     'ASING' => $this->input_post('ASING_PS'),
                //     'JMLPELANGGAN' => $this->input_post('JMLPELANGGAN'),
                //     );
                //
                //   $data_materisiaran = array(
                //     'LOKAL' => $this->input_post('LOKAL_PMS'),
                //     'ASING' => $this->input_post('ASING_PMS'),
                //     );
                //
                //   $data_waktusiaran = array(
                //     'HARIKERJAAWAL' => $this->input_post('HARIKERJAAWAL'),
                //     'HARILIBURAWAL' => $this->input_post('HARILIBURAWAL'),
                //     'HARIKERJAAKHIR' => $this->input_post('HARIKERJAAKHIR'),
                //     'HARILIBURAKHIR' => $this->input_post('HARILIBURAKHIR'),
                //     );
                //
                //
                // $komposisi = array (
                //   'WNI' => $this->input_post('WNI'),
                //   'WNA' => $this->input_post('WNA'),
                //   );

                // $data = array(
                //   'IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL,
                //   'NMPERUSAHAAN' => $this->input_post('NMPERUSAHAAN'),
                //   'NMUDARA' => $this->input_post('NMUDARA'),
                //   );

                // $data_ipp_prinsip = array(
                //   'id_perusahaan' => $IDPERUSAHAANFINAL,
                //   'no_ipp_prinsip' => $this->input_post('NOSURAT_IPP_PRINSIP'),
                //   'tgl_ipp_prinsip' => ymd_format($this->input_post('TANGGALSURAT_IPP_PRINSIP')),
                //   'tgl_perpanjangan' => ymd_format($this->input_post('TANGGALSURAT_IPP_PRINSIP_BERAKHIR')),
                // );
                //
                // $data_ipp_tetap = array(
                //   'id_perusahaan' => $IDPERUSAHAANFINAL,
                //   'no_ipp_tetap' => $this->input_post('NOSURAT_IPP_TETAP'),
                //   'tgl_ipp_tetap' => ymd_format($this->input_post('TANGGALSURAT_IPP_TETAP')),
                //   'tgl_perpanjangan' => ymd_format($this->input_post('TANGGALSURAT_IPP_TETAP_BERAKHIR')),
                // );

                /* transaction begin */

              // $this->db->trans_begin();

              /* update data*/

              // if(!empty($data_ipp_prinsip)) {
              //   $this->db->where('id_perusahaan',$IDPERUSAHAANFINAL);
              //   $this->db->update('tbl_ipp_prinsip', $data_ipp_prinsip);
              // }
              //
              // if(!empty($data_ipp_tetap)) {
              //   $this->db->where('id_perusahaan',$IDPERUSAHAANFINAL);
              //   $this->db->update('tbl_ipp_tetap', $data_ipp_tetap);
              // }
              //
              // if(!empty($data_pengecekan)) {
              //   $this->db->where('id_perusahaan',$IDPERUSAHAANFINAL);
              //   $this->db->update('tbl_pengecekan', $data_pengecekan);
              // }
              // if (!empty($komposisi)) {
              //   $this->db->where('IDPERUSAHAANFINAL', $IDPERUSAHAANFINAL);
              //   $this->db->update('komposisi_pemegang_saham', $komposisi);
              // }
              // if (!empty($data_sumbermaterilpb)) {
              //   for($i = 0; $i < count($data_sumbermaterilpb); $i++) {
              //     $data_sumbermaterilpb[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('sumbermaterisiaranlpb', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('sumbermaterisiaranlpb', $data_sumbermaterilpb);
              // }
              // if (!empty($data_totalkanalberita)) {
              //   for($i = 0; $i < count($data_totalkanalberita); $i++) {
              //     $data_totalkanalberita[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanalberita', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanalberita', $data_totalkanalberita);
              // }
              // if (!empty($data_iklankomersial)) {
              //   for($i = 0; $i < count($data_iklankomersial); $i++) {
              //     $data_iklankomersial[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('iklankomersial', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('iklankomersial', $data_iklankomersial);
              // }
              // if (!empty($data_totalkanalumum)) {
              //   for($i = 0; $i < count($data_totalkanalumum); $i++) {
              //     $data_totalkanalumum[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanalumum', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanalumum', $data_totalkanalumum);
              // }
              // if (!empty($data_totalkanaldakwah)) {
              //   for($i = 0; $i < count($data_totalkanaldakwah); $i++) {
              //     $data_totalkanaldakwah[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanaldakwah', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanaldakwah', $data_totalkanaldakwah);
              // }
              // if (!empty($data_totalkanalfilm)) {
              //   for($i = 0; $i < count($data_totalkanalfilm); $i++) {
              //     $data_totalkanalfilm[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanalfilm', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanalfilm', $data_totalkanalfilm);
              // }
              // if (!empty($data_totalkanallainnya)) {
              //   for($i = 0; $i < count($data_totalkanallainnya); $i++) {
              //     $data_totalkanallainnya[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanallainnya', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanallainnya', $data_totalkanallainnya);
              // }
              // if (!empty($data_totalkanalmusik)) {
              //   for($i = 0; $i < count($data_totalkanalmusik); $i++) {
              //     $data_totalkanalmusik[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanalmusik', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanalmusik', $data_totalkanalmusik);
              // }
              // if (!empty($data_totalkanalolahraga)) {
              //   for($i = 0; $i < count($data_totalkanalolahraga); $i++) {
              //     $data_totalkanalolahraga[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanalolahraga', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanalolahraga', $data_totalkanalolahraga);
              // }
              // if (!empty($data_totalkanalpendidikan)) {
              //   for($i = 0; $i < count($data_totalkanalpendidikan); $i++) {
              //     $data_totalkanalpendidikan[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('totalkanalpendidikan', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('totalkanalpendidikan', $data_totalkanalpendidikan);
              // }
              // if (!empty($data_pemusatankepemilikansilang)) {
              //   for($i = 0; $i < count($data_pemusatankepemilikansilang); $i++) {
              //     $data_pemusatankepemilikansilang[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //   }
              //   $this->db->delete('pemusatankepemilikansilang', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //   $this->db->insert_batch('pemusatankepemilikansilang', $data_pemusatankepemilikansilang);
              // }
              //
              //     if (!empty($data_pemegangsaham)) {
              //         for($i = 0; $i < count($data_pemegangsaham); $i++) {
              //             $data_pemegangsaham[$i]['IDPERUSAHAANFINAL'] = $IDPERUSAHAANFINAL;
              //         }
              //         $this->db->delete('pemegangsaham', array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //         $this->db->insert_batch('pemegangsaham', $data_pemegangsaham);
              //     }
              //
              // $this->db->update('users', $data_users, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //
              // $this->db->update('penyelenggara', $data, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //
              // if (!empty($data_studio)) {
              //   $this->db->update('alamatstudio', $data_studio, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pemancar)) {
              //   $this->db->update('alamatpemancar', $data_pemancar, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_contact_person)) {
              //   $this->db->update('contactperson', $data_contact_person, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_alamatkantor)) {
              //   $this->db->update('alamatkantor', $data_alamatkantor, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_aktapendirian)) {
              //   $this->db->update('aktapendirian', $data_aktapendirian, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_aspekmodallpb)) {
              //   $this->db->update('aspekmodallpb', $data_aspekmodallpb, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_aspekpermodalan)) {
              //   $this->db->update('aspekpermodalan', $data_aspekpermodalan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_aspekpermodalankom)) {
              //   $this->db->update('aspekpermodalankom', $data_aspekpermodalankom, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_aspekpermodalankomsumber)) {
              //   $this->db->update('aspekmodalkomsumber', $data_aspekpermodalankomsumber, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_stasiun)) {
              //   $this->db->update('alamatstasiunpengendali', $data_stasiun, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dppemberitaan)) {
              //   $this->db->update('dppemberitaan', $data_dppemberitaan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dpsiaran)) {
              //   $this->db->update('dpsiaran', $data_dpsiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dptatausaha)) {
              //   $this->db->update('dptatausaha', $data_dptatausaha, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dpteknikstudio)) {
              //   $this->db->update('dpteknikstudio', $data_dpteknikstudio, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dptekniktransmisi)) {
              //   $this->db->update('dptekniktransmisi', $data_dptekniktransmisi, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dptotalpengurus)) {
              //   $this->db->update('dptotalpengurus', $data_dptotalpengurus, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dpk)) {
              //   $this->db->update('ketuadpk', $data_dpk, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_adpk)) {
              //   $this->db->update('anggotadpk', $data_adpk, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_dpengawas)) {
              //   $this->db->update('ketuadpengawas', $data_dpengawas, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_adpengawas)) {
              //   $this->db->update('anggotadpengawas', $data_adpengawas, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_adpengawas2)) {
              //   $this->db->update('anggotadpengawas2', $data_adpengawas2, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pendiri)){
              //   $this->db->update('pendiri', $data_pendiri, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //         }
              // if (!empty($data_ketuapendiri)) {
              //   $this->db->update('ketuapendiri', $data_ketuapendiri, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_direktur)) {
              //   $this->db->update('direktur', $data_direktur, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_direktur_2)) {
              //   $this->db->update('direktur_2', $data_direktur_2, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_komisarisutama)) {
              //   $this->db->update('komisarisutama', $data_komisarisutama, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_komisaris)) {
              //   $this->db->update('komisaris', $data_komisaris, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_komisaris2)) {
              //   $this->db->update('komisaris2', $data_komisaris2, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pjbidusaha)){
              //   $this->db->update('pjbidusaha', $data_pjbidusaha, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //         }
              // if (!empty($data_pjbidkeu)) {
              //   $this->db->update('pjbidkeu', $data_pjbidkeu, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pjbidpemberitaan)) {
              //   $this->db->update('pjbidpemberitaan', $data_pjbidpemberitaan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pjbidsiaran)) {
              //   $this->db->update('pjbidsiaran', $data_pjbidsiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pjbidteknik)) {
              //   $this->db->update('pjbidteknik', $data_pjbidteknik, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_pendirian_lpplokal)) {
              //   $this->db->update('pendirian_lpplokal', $data_pendirian_lpplokal, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_peng_akta_pendirian)) {
              //   $this->db->update('peng_aktapendirian', $data_peng_akta_pendirian, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_peng_akta_perubahan)) {
              //   $this->db->update('peng_aktaperubahan', $data_peng_akta_perubahan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_aktaperubahan)) {
              //   $this->db->update('aktaperubahan', $data_aktaperubahan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_surat_ket_domisili)) {
              //   $this->db->update('surat_ket_domisili', $data_surat_ket_domisili, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_sistem_modulasi)) {
              //   $this->db->update('sistemmodulasi', $data_sistem_modulasi, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_stasiun_distribusi)) {
              //   $this->db->update('stasiundistribusi', $data_stasiun_distribusi, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_stasiun_pemancar)) {
              //   $this->db->update('stasiunpemancar', $data_stasiun_pemancar, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_speksatelit)) {
              //   $this->db->update('speksatelit', $data_speksatelit, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_spekradio)) {
              //   $this->db->update('spekperlradio', $data_spekradio, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_transponder)) {
              //   $this->db->update('transponder', $data_transponder, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_perlpemancar)) {
              //   $this->db->update('peralatanpemancar', $data_perlpemancar, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_feeder)) {
              //   $this->db->update('feeder', $data_feeder, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_satelitpenyiaran)) {
              //   $this->db->update('satelitpenyiaran', $data_satelitpenyiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_formatsiaran)) {
              //   $this->db->update('formatsiaran`', $data_formatsiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_surveyjk)) {
              //   $this->db->update('surveyjk', $data_surveyjk, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_surveyusia)) {
              //   $this->db->update('surveyusia', $data_surveyusia, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_surveypekerjaan)) {
              //   $this->db->update('surveypekerjaan', $data_surveypekerjaan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_surveypendidikan)) {
              //   $this->db->update('surveypendidikan', $data_surveypendidikan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_surveyekonomi)) {
              //   $this->db->update('surveyekonomi', $data_surveyekonomi, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_sumbermateri)) {
              //   $this->db->update('sumbermateri', $data_sumbermateri, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              //         }
              // if (!empty($data_waktusiaranlpb)) {
              //   $this->db->update('waktusiaranlpb', $data_waktusiaranlpb, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_persentasesiaranlpb)) {
              //   $this->db->update('persentasematasiaran', $data_persentasesiaranlpb, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_persentasesiaran)) {
              //   $this->db->update('persentasesiaran', $data_persentasesiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_persentasesiaranmusik)) {
              //   $this->db->update('persentasesiaranmusik', $data_persentasesiaranmusik, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_lpbpersentasesiaran)) {
              //   $this->db->update('persentasesiaranlpb', $data_lpbpersentasesiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_materisiaran)) {
              //   $this->db->update('materisiaran', $data_materisiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_waktusiaran)) {
              //   $this->db->update('waktusiaran', $data_waktusiaran, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }
              // if (!empty($data_wilayahlayanan)) {
              //   $this->db->update('wilayahlayanan', $data_wilayahlayanan, array('IDPERUSAHAANFINAL' => $IDPERUSAHAANFINAL));
              // }

              /* end transaction */

    //           if ($this->db->trans_status() === FALSE){
    //             //rollback if error
    //               $this->db->trans_rollback();
    //               return FALSE;
    //           }else{
    //             //commit data
    //               $this->db->trans_commit();
    //               // $get_lampiran =  $this->get_dropzone_temporary($id, get_lembaga_id($id), $mode = '2');
    //               // $move_lampiran = $this->move_lampiran($id, $mode = '2');
    //
    //               // $IDPERUSAHAANFINAL = get_lembaga_id($id_perusahaan);
    //               $data = array(
    //                   'status_evaluasi'  => '201',
    //                   );
    //               $this->db->where('status_evaluasi','200');
    //               $this->db->update('tbl_perubahan_otik', $data);
    //
    //               return TRUE;
    //           }
    //
    //         return FALSE;
    //
    // }

}
