<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_ipp_tetap extends Model {
    protected $table = 'tbl_ipp_tetap';
    protected $primaryKey = 'id_perusahaan';

    protected $fillable = [
      'id_ipp_tetap',
      'no_ipp_tetap',
      'id_perusahaan',
      'tgl_ipp_tetap',
      'tgl_perpanjangan',
      'tgl_penetapan',
      'ipp_tetap',
      'tgl_ipp_tetap_pjg',
      'status_ipp_tetap',
      'approval',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
