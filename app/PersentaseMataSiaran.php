<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PersentaseMataSiaran extends Model {
    protected $table = 'persentasematasiaran';
    protected $primaryKey = 'IDPERUSAHAANFINAL';

    protected $fillable = [
      'NO',
      'IDPERUSAHAANFINAL',
      'UMUM',
      'BERITA',
      'PENDIDIKAN',
      'AGAMA',
      'OLAHRAGA',
      'FILM',
      'MUSIK',
      'LAINNYA',
      'tgl_perpanjangan',
      'tgl_perubahan',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
