<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_ipp_prinsip extends Model {
    protected $table = 'tbl_ipp_prinsip';
    protected $primaryKey = 'id_perusahaan';

    protected $fillable = [
      'id_ipp_prinsip',
      'no_ipp_prinsip',
      'id_perusahaan',
      'tgl_ipp_prinsip',
      'tgl_perpanjangan',
      'ipp_prinsip',
      'ipp_prinsip_ttd',
      'status_ipp_prinsip',
      'approval',

  ];

    public $timestamps = false;

    // public static $rules = [
    //     'NO' => 'required',
    //     'IDPERUSAHAANFINAL' => 'required',
    //     'NMPERUSAHAAN' => 'required',
    //     'LEMBAGASIAR' => 'required',
    //     'ALAMATKANTOR' => 'required',
    //     'ALAMATPENAGIHAN' => 'required',
    //     'PROPKANTOR' => 'required',
    //     'KABKANTOR' => 'required',
    //     'KECKANTOR' => 'required',
    //     'KELKANTOR' => 'required',
    //     'KODEPOS' => 'required',
    //     'NPWP' => 'required',
    //     'STATUS' => 'required',
    //     'NOTELPKANTOR' => 'required',
    //     'NOHPKANTOR' => 'required',
    //     'WEBSITE' => 'required',
    //     'ZONA' => 'required',
    //     'tgl_perpanjangan' => 'required',
    //     'tgl_perubahan' => 'required',
    //
    // ];

}
