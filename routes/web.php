
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
// $app->group(
//     ['middleware' => 'jwt.auth'],
//     function() use ($app) {
//         $app->get('users', function() {
//             $users = \App\User::all();
//             return response()->json($users);
//         });
//     }
// );
// $app->post('auth/login',['uses' => 'AuthController@authenticate']
// );
// $app->group(['prefix' => 'api'], function () use ($app) {
//     //status route
//     $app->get('status', 'StatusController@index');
//     $app->get('status/{id}', 'StatusController@getStatus');
//     $app->post('auth/login', 'AuthController@postLogin');
//     // $app->get('status/{id}', 'StatusController@read');
//     // $app->get('products/{id}', 'ProductController@show');
//     // $app->post('products', 'ProductController@store');
//     // $app->put('products/{id}', 'ProductController@update');
//     // $app->delete('products/{id}', 'ProductController@delete');
//
//
// });
$app->post('auth/login', 'AuthController@login');
$app->group([
  'middleware' => 'auth:api',
//   'namespace' => 'App\Http\Controllers',
     'prefix' => 'statusAPI'
], function($app)
{
    $app->get('/test', function() {
        return response()->json([
            'message' => 'Hello World!',
        ]);
    });
    // $app->post('auth/login', 'AuthController@postLogin');
    // $app->post('auth/logout', 'AuthController@postLogout');
    // $app->post('auth/login', 'AuthController@login');
    $app->post('auth/logout', 'AuthController@logout');
    $app->post('auth/refresh', 'AuthController@refresh');
    $app->get('status', 'StatusController@index');
    $app->get('status/{id}', 'StatusController@getStatus');
});

$app->group([
  // 'middleware' => 'auth:api',
//   'namespace' => 'App\Http\Controllers',
     'prefix' => 'perubahanDataAPI'
], function($app)
{
    $app->get('/test', function() {
        return response()->json([
            'message' => 'Hello World!',
        ]);
    });
    $app->post('auth/logout', 'AuthController@logout');
    $app->post('auth/refresh', 'AuthController@refresh');
    // $app->get('status', 'StatusController@index');
    // $app->get('status/{id}', 'StatusController@getStatus');
    //contact person
    $app->post('contact_person/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@updateContactPerson');

    //masih error surveyusia
    $app->post('SLPB/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@updateSLPB');
    // //masih error ipp prinsip->sip
    // $app->post('ipp_prinsip/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@updateIPP_prinsip');
    // //test penyelenggara
    // $app->post('penyelenggara/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@updatePenyelenggara');
    // // alamat pemancar
    // // $app->post('alamat_pemancar/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@updatealamatPemancar');
    // //alamat studio
    // $app->post('alamat_studio/updateAlamatStudio/{IDPERUSAHAANFINAL}', 'AlamatStudioController@updateAlamatStudio');
    //
    // //alamat studio
    // $app->post('alamat_studio/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@updateAlamatStudio');

$app->post('ipp_tetap/update/{IDPERUSAHAANFINAL}', 'MenuPerubahanData@update_tbl_ipp_tetap');
});
